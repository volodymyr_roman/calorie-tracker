INSERT INTO roles (id, name)
VALUES
  (1, 'ADMIN'),
  (2, 'USER_MANAGER'),
  (3, 'USER');


INSERT INTO user_permissions (id, permission, role_id)
VALUES
  (1, 'EDIT_ALL_USER_RECORDS', 1),
  (2, 'VIEW_ALL_USER_RECORDS', 1),
  (3, 'EDIT_ALL_MEAL_RECORDS', 1),
  (4, 'VIEW_ALL_MEAL_RECORDS', 1),
  (5, 'VIEW_ALL_USER_RECORDS', 2),
  (6, 'EDIT_ALL_USER_RECORDS', 2),
  (7, 'EDIT_OWN_USER_RECORDS', 3),
  (8, 'VIEW_OWN_USER_RECORDS', 3),
  (9, 'VIEW_OWN_MEAL_RECORDS', 3),
  (10, 'EDIT_OWN_MEAL_RECORDS', 3);


INSERT INTO users (id, email, password)
VALUES
  (1, 'admin@mail.com', '$2a$04$hSUqsqN.yu4VOl8mb/wirelIfJueon8OJW4gtvy9tmjHv51Mzm2bS');


INSERT INTO users_roles (user_id, role_id)
VALUES
  (1, 1);

