package fine.project.calorietracker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Entry point for application.
 *
 * @author Volodymyr Roman
 */
@SpringBootApplication
public class CalorieTrackerApplication {

	/**
	 * Entry method for application.
	 *
	 * @param args console parameters that can override default properties.
	 */
	public static void main(final String[] args) {
		SpringApplication.run(CalorieTrackerApplication.class, args);
	}
}
