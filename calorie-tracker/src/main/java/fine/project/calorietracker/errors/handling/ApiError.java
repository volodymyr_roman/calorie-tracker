package fine.project.calorietracker.errors.handling;

import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.util.Collections;
import java.util.List;

/**
 * Object that is transferred when error happens.
 *
 * @author Volodymyr Roman
 */
@Getter
public class ApiError {

	private final HttpStatus status;

	private final String message;

	private final List<String> errors;

	public ApiError(final HttpStatus status, final String message, final List<String> errors) {
		super();
		this.status = status;
		this.message = message;
		this.errors = errors;
	}

	public ApiError(final HttpStatus status, final String message, final String error) {
		super();
		this.status = status;
		this.message = message;
		this.errors = Collections.singletonList(error);
	}

	public ApiError(final HttpStatus status, final String message) {
		super();
		this.status = status;
		this.message = message;
		this.errors = null;
	}
}
