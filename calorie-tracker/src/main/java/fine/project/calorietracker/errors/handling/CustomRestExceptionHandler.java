package fine.project.calorietracker.errors.handling;

import org.antlr.v4.runtime.misc.ParseCancellationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Custom exception handling with {@link ApiError} object.
 *
 * @author Volodymyr Roman
 */
@ControllerAdvice
public class CustomRestExceptionHandler extends ResponseEntityExceptionHandler {

	/**
	 * Handle {@link HttpMessageNotReadableException}.
	 */
	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(final HttpMessageNotReadableException ex,
			final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
		final ApiError httpMessageNotReadable = new ApiError(HttpStatus.BAD_REQUEST, "Http message not readable",
				ex.getLocalizedMessage());
		return handleExceptionInternal(ex, httpMessageNotReadable, headers, httpMessageNotReadable.getStatus(),
				request);
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(final MethodArgumentNotValidException ex,
			final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
		final List<String> errors = new ArrayList<>();
		for (final FieldError error : ex.getBindingResult().getFieldErrors()) {
			errors.add(error.getField() + ": " + error.getDefaultMessage());
		}
		for (final ObjectError error : ex.getBindingResult().getGlobalErrors()) {
			errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
		}
		final ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST,
				"Validation fail for " + ex.getParameter().getParameterName(), errors);
		return handleExceptionInternal(ex, apiError, headers, apiError.getStatus(), request);
	}

	@Override
	protected ResponseEntity<Object> handleMissingServletRequestParameter(
			final MissingServletRequestParameterException ex, final HttpHeaders headers, final HttpStatus status,
			final WebRequest request) {
		final String error = ex.getParameterName() + " parameter is missing";
		final ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), error);
		return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
	}

	/**
	 * Handle {@link ConstraintViolationException}.
	 */
	@ExceptionHandler({ConstraintViolationException.class})
	public ResponseEntity<Object> handleConstraintViolation(final ConstraintViolationException ex,
			final WebRequest request) {
		final List<String> errors = new ArrayList<>();
		for (final ConstraintViolation<?> violation : ex.getConstraintViolations()) {
			errors.add(violation.getRootBeanClass().getName() + " " + violation.getPropertyPath() + ": " +
					violation.getMessage());
		}
		final ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), errors);
		return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
	}

	/**
	 * Handle {@link DataIntegrityViolationException}. Exception happends when there are unique constraint.
	 */
	@ExceptionHandler({DataIntegrityViolationException.class})
	public ResponseEntity<Object> handleConstraintViolation(final DataIntegrityViolationException ex,
			final WebRequest request) {
		Throwable sqlException = ex;
		while (sqlException != null && !SQLException.class.isInstance(sqlException)) {
			sqlException = sqlException.getCause();
		}
		final ApiError apiError;
		if (sqlException == null) {
			apiError = new ApiError(HttpStatus.CONFLICT, ex.getLocalizedMessage());
		} else {
			apiError = new ApiError(HttpStatus.CONFLICT, ex.getCause().getLocalizedMessage(),
					sqlException.getLocalizedMessage());
		}
		return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
	}

	/**
	 * Handle {@link MethodArgumentTypeMismatchException}.
	 */
	@ExceptionHandler({MethodArgumentTypeMismatchException.class})
	public ResponseEntity<Object> handleMethodArgumentTypeMismatch(final MethodArgumentTypeMismatchException ex,
			final WebRequest request) {
		final String error = ex.getName() + " should be of type " + ex.getRequiredType().getName();
		final ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), error);
		return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
	}

	/**
	 * Handle {@link NoHandlerFoundException }.
	 */
	@Override
	protected ResponseEntity<Object> handleNoHandlerFoundException(final NoHandlerFoundException ex,
			final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
		final String error = "No handler found for " + ex.getHttpMethod() + " " + ex.getRequestURL();
		final ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, ex.getLocalizedMessage(), error);
		return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
	}

	/**
	 * Handle {@link HttpRequestMethodNotSupportedException}.
	 */
	@Override
	protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(
			final HttpRequestMethodNotSupportedException ex, final HttpHeaders headers, final HttpStatus status,
			final WebRequest request) {
		final StringBuilder builder = new StringBuilder();
		builder.append(ex.getMethod());
		builder.append(" method is not supported for this request. Supported methods are ");
		ex.getSupportedHttpMethods().forEach(t -> builder.append(t).append(" "));
		final ApiError apiError = new ApiError(HttpStatus.METHOD_NOT_ALLOWED, ex.getLocalizedMessage(),
				builder.toString());
		return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
	}

	/**
	 * Handle {@link HttpMediaTypeNotSupportedException}.
	 */
	@Override
	protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(final HttpMediaTypeNotSupportedException ex,
			final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
		final StringBuilder builder = new StringBuilder();
		builder.append(ex.getContentType());
		builder.append(" media type is not supported. Supported media types are ");
		ex.getSupportedMediaTypes().forEach(t -> builder.append(t).append(", "));

		final ApiError apiError = new ApiError(HttpStatus.UNSUPPORTED_MEDIA_TYPE, ex.getLocalizedMessage(),
				builder.substring(0, builder.length() - 2));
		return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
	}

	/**
	 * Handle {@link AccessDeniedException}.
	 */
	@ExceptionHandler({AccessDeniedException.class})
	public ResponseEntity<Object> handleAccessDenied(final AccessDeniedException ex, final WebRequest request) {
		final ApiError apiError = new ApiError(HttpStatus.FORBIDDEN, ex.getLocalizedMessage(),
				"User don't have authority to access.");
		return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
	}

	/**
	 * Antlr exception handling.
	 */
	@ExceptionHandler({ParseCancellationException.class})
	public ResponseEntity<Object> handleParseCancelationException(final ParseCancellationException ex,
			final WebRequest request) {
		final ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, "Syntax error", ex.getLocalizedMessage());
		return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
	}

	/**
	 * Handle all exceptions and also anotated with {@link ResponseStatus}.
	 */
	@ExceptionHandler({PropertyReferenceException.class})
	public ResponseEntity<Object> handlePropertyReferenceException(final PropertyReferenceException ex) {
		final ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(),
				"Property '" + ex.getPropertyName() + "' is invalid");
		return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
	}

	/**
	 * Handle all exceptions and also annotated with {@link ResponseStatus}.
	 */
	@ExceptionHandler({Exception.class})
	public ResponseEntity<Object> handleAll(final Exception ex, final WebRequest request) {
		final ResponseStatus responseStatus = ex.getClass().getAnnotation(ResponseStatus.class);
		if (responseStatus != null) {
			final ApiError apiError = new ApiError(responseStatus.value(), ex.getLocalizedMessage());
			return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
		} else {
			final ApiError apiError = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, ex.getLocalizedMessage(),
					"error occurred");
			return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
		}
	}
}
