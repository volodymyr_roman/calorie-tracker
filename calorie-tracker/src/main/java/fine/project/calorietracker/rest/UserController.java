package fine.project.calorietracker.rest;

import fine.project.calorietracker.dto.UserDTO;
import fine.project.calorietracker.dto.UserResource;
import fine.project.calorietracker.security.CustomUserDetails;
import fine.project.calorietracker.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * User controller.
 *
 * @author Volodymyr Roman
 */
@RestController
@Api(tags = "user", description = "User operations")
@RequestMapping("api/users")
public class UserController {

	private final UserService userService;

	public UserController(final UserService userService) {
		this.userService = userService;
	}

	/**
	 * Create user.
	 *
	 * @param userDTO user that needs to be saved
	 * @return non null response
	 */
	@PostMapping
	@ResponseStatus(value = HttpStatus.CREATED)
	UserResource createUser(@Validated @RequestBody final UserDTO userDTO) {
		return this.userService.createUser(userDTO);
	}

	/**
	 * Get user by id.
	 *
	 * @param userId user id
	 * @return non null response
	 */
	@ApiImplicitParam(name = "Authorization", paramType = "header", example = "Bearer {token}")
	@PreAuthorize("hasAuthority('VIEW_ALL_USER_RECORDS') or " +
			"(hasAuthority('VIEW_OWN_USER_RECORDS') and #userId == principal.userId)")
	@ApiOperation(value = "Get user by id")
	@GetMapping("/{userId}")
	@ResponseStatus(value = HttpStatus.OK)
	UserResource getUser(@PathVariable(name = "userId") final Long userId) {
		return this.userService.getUser(userId);
	}

	/**
	 * Search users with pagination.
	 */
	@ApiOperation(value = "Search user records")
	@ApiImplicitParams({@ApiImplicitParam(name = "Authorization", paramType = "header",
			example = "Bearer {token}"),
			@ApiImplicitParam(name = "page", dataType = "integer", paramType = "query",
					value = "Results page you want to retrieve (0..N)"),
			@ApiImplicitParam(name = "size", dataType = "integer", paramType = "query",
					value = "Number of records per page."),
			@ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string",
					paramType = "query",
					value = "Sorting criteria in the format: property(,asc|desc). " +
							"Default sort order is ascending. " +
							"Multiple sort criteria are supported.")})
	@PreAuthorize("hasAuthority('VIEW_ALL_USER_RECORDS')")
	@GetMapping
	@ResponseStatus(value = HttpStatus.OK)
	Page<UserResource> searchUsers(@ApiIgnore final Pageable pageable, final String searchQuery) {
		return this.userService.searchUsers(pageable, searchQuery);
	}

	/**
	 * Update user.
	 *
	 * @param userId user id
	 * @param userDTO user info that needs to be updated
	 * @return non null response
	 */
	@ApiImplicitParam(name = "Authorization", paramType = "header", example = "Bearer {token}")
	@PreAuthorize("hasAuthority('EDIT_ALL_USER_RECORDS') or " +
			"(hasAuthority('EDIT_OWN_USER_RECORDS') and #userId == principal.userId)")
	@PutMapping("/{userId}")
	@ResponseStatus(value = HttpStatus.OK)
	UserResource updateUser(@PathVariable(name = "userId") final Long userId,
			@Validated @RequestBody final UserDTO userDTO) {
		return this.userService.updateUser(userId, userDTO);
	}

	/**
	 * Delete user and all related records.
	 *
	 * @param userId user id
	 */
	@ApiImplicitParam(name = "Authorization", paramType = "header", example = "Bearer {token}")
	@PreAuthorize("hasAuthority('EDIT_ALL_USER_RECORDS') and (#userId != principal.userId)")
	@DeleteMapping("/{userId}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	void deleteUser(@PathVariable(name = "userId") final Long userId) {
		this.userService.deleteUser(userId);
	}

	/**
	 * Retrieves current user.
	 */
	@ApiImplicitParam(name = "Authorization", paramType = "header", example = "Bearer {token}")
	@PreAuthorize("isAuthenticated()")
	@GetMapping("/current")
	@ResponseStatus(value = HttpStatus.OK)
	UserResource getCurrentUser(final Authentication authentication) {
		final CustomUserDetails userDetails = (CustomUserDetails) authentication.getPrincipal();
		return this.userService.getUser(userDetails.getUserId());
	}
}
