package fine.project.calorietracker.rest;

import fine.project.calorietracker.dto.MealDTO;
import fine.project.calorietracker.dto.MealResource;
import fine.project.calorietracker.service.MealService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Meal controller.
 *
 * @author Volodymyr Roman
 */
@RestController
@Api(tags = "meals", description = "Meals operations")
@RequestMapping("api/users/{userId}/meals")
public class MealController {

	private final MealService mealService;

	public MealController(final MealService mealService) {
		this.mealService = mealService;
	}

	/**
	 * Create meal for specific user.
	 *
	 * @param userId user id
	 * @param mealDTO meal that needs to be saved
	 * @return non null response
	 */
	@ApiImplicitParam(name = "Authorization", paramType = "header", example = "Bearer {token}")
	@PreAuthorize("hasAuthority('EDIT_ALL_MEAL_RECORDS') or " +
			"(hasAuthority('EDIT_OWN_MEAL_RECORDS') and #userId == principal.userId)")
	@PostMapping
	@ResponseStatus(value = HttpStatus.CREATED)
	MealResource createMeal(@PathVariable(name = "userId") final Long userId,
			@Validated @RequestBody final MealDTO mealDTO) {
		return this.mealService.createMeal(userId, mealDTO);
	}

	/**
	 * Get meal for specific user.
	 *
	 * @param userId user id
	 * @param mealId meal id
	 * @return non null response
	 */
	@ApiImplicitParam(name = "Authorization", paramType = "header", example = "Bearer {token}")
	@ApiOperation(value = "Get meal by id")
	@PreAuthorize("hasAuthority('VIEW_ALL_MEAL_RECORDS') or " +
			"(hasAuthority('VIEW_OWN_MEAL_RECORDS') and #userId == principal.userId)")
	@GetMapping("/{mealId}")
	@ResponseStatus(value = HttpStatus.OK)
	MealResource getMeal(@PathVariable(name = "userId") final Long userId,
			@PathVariable(name = "mealId") final Long mealId) {
		return this.mealService.getMeal(userId, mealId);
	}

	/**
	 * Search meals by query with pagination. E.g of query date eq '2016-05-01'.
	 *
	 * @param userId user id
	 * @param searchQuery search query
	 * @param pageable page request
	 * @return non null response
	 */
	@ApiOperation(value = "Search meals record")
	@ApiImplicitParams({@ApiImplicitParam(name = "Authorization", paramType = "header", example = "Bearer {token}"),
			@ApiImplicitParam(name = "userId", required = true, dataType = "string",
					paramType = "path"),
			@ApiImplicitParam(name = "searchQuery", dataType = "string", paramType = "query"),
			@ApiImplicitParam(name = "page", dataType = "integer", paramType = "query",
					value = "Results page you want to retrieve (0..N)"),
			@ApiImplicitParam(name = "size", dataType = "integer", paramType = "query",
					value = "Number of records per page."),
			@ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string",
					paramType = "query",
					value = "Sorting criteria in the format: pruseroperty(,asc|desc). " +
							"Default sort order is ascending. " +
							"Multiple sort criteria are supported.")})
	@PreAuthorize("hasAuthority('VIEW_ALL_MEAL_RECORDS') or " +
			"(hasAuthority('VIEW_OWN_MEAL_RECORDS') and #userId == principal.userId)")
	@GetMapping
	@ResponseStatus(value = HttpStatus.OK)
	Page<MealResource> searchMeals(@ApiIgnore @PathVariable(name = "userId") final Long userId,
			@ApiIgnore final Pageable pageable, @ApiIgnore final String searchQuery) {
		return this.mealService.searchMeals(userId, pageable, searchQuery);
	}

	/**
	 * Update meal for specific user.
	 *
	 * @param userId user id
	 * @param mealDTO meal that needs to be saved
	 * @param mealId meal id that will be updated
	 * @return non null response
	 */
	@ApiImplicitParam(name = "Authorization", paramType = "header", example = "Bearer {token}")
	@PreAuthorize("hasAuthority('EDIT_ALL_MEAL_RECORDS') or " +
			"(hasAuthority('EDIT_OWN_MEAL_RECORDS') and #userId == principal.userId)")
	@PutMapping("/{mealId}")
	@ResponseStatus(value = HttpStatus.OK)
	MealResource updateMeal(@PathVariable(name = "userId") final Long userId,
			@PathVariable(name = "mealId") final Long mealId, @Validated @RequestBody final MealDTO mealDTO) {
		return this.mealService.updateMeal(userId, mealId, mealDTO);
	}

	/**
	 * Delete meal for specific user.
	 *
	 * @param userId user id
	 * @param mealId meal id
	 */
	@ApiImplicitParam(name = "Authorization", paramType = "header", example = "Bearer {token}")
	@PreAuthorize("hasAuthority('EDIT_ALL_MEAL_RECORDS') or " +
			"(hasAuthority('EDIT_OWN_MEAL_RECORDS') and #userId == principal.userId)")
	@DeleteMapping("/{mealId}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	void deleteMeal(@PathVariable(name = "userId") final Long userId,
			@PathVariable(name = "mealId") final Long mealId) {
		this.mealService.deleteMeal(userId, mealId);
	}
}
