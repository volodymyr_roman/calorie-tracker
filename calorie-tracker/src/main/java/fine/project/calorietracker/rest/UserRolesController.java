package fine.project.calorietracker.rest;

import fine.project.calorietracker.dto.UserRolesResource;
import fine.project.calorietracker.security.CustomUserDetails;
import fine.project.calorietracker.service.UserRolesService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * User role controller.
 *
 * @author Volodymyr Roman
 */
@RestController
@Api(tags = "roles", description = "User roles operations")
@RequestMapping("api/users")
public class UserRolesController {

	private final UserRolesService userRolesService;

	@Autowired
	public UserRolesController(final UserRolesService userRolesService) {
		this.userRolesService = userRolesService;
	}

	/**
	 * Retrieves current user.
	 */
	@ApiImplicitParam(name = "Authorization", paramType = "header", example = "Bearer {token}")
	@PreAuthorize("hasAuthority('EDIT_ALL_USER_RECORDS')")
	@PostMapping("/{userId}/roles/{role}")
	@ResponseStatus(value = HttpStatus.OK)
	UserRolesResource addRole(@PathVariable(name = "userId") final Long userId,
			@PathVariable("role") final String role) {
		return this.userRolesService.addRoleToUser(userId, role);
	}

	/**
	 * Retrieves current user.
	 */
	@ApiImplicitParam(name = "Authorization", paramType = "header", example = "Bearer {token}")
	@PreAuthorize("hasAuthority('EDIT_ALL_USER_RECORDS')")
	@DeleteMapping("/{userId}/roles/{role}")
	@ResponseStatus(value = HttpStatus.OK)
	UserRolesResource deleteRole(@PathVariable(name = "userId") final Long userId,
			@PathVariable("role") final String role) {
		return this.userRolesService.deleteUserRole(userId, role);
	}

	/**
	 * Get user roles.
	 */
	@ApiImplicitParam(name = "Authorization", paramType = "header", example = "Bearer {token}")
	@PreAuthorize("hasAuthority('VIEW_ALL_USER_RECORDS') or " +
			"(hasAuthority('VIEW_OWN_USER_RECORDS') and #userId == principal.userId)")
	@GetMapping("/{userId}/roles")
	@ResponseStatus(value = HttpStatus.OK)
	UserRolesResource getUserRoles(@PathVariable(name = "userId") final Long userId) {
		return this.userRolesService.getUserRoles(userId);
	}

	/**
	 * Retrieves current user.
	 */
	@PreAuthorize("isAuthenticated()")
	@GetMapping("/current/roles")
	@ResponseStatus(value = HttpStatus.OK)
	UserRolesResource getCurrentUserROles(final Authentication authentication) {
		final CustomUserDetails userDetails = (CustomUserDetails) authentication.getPrincipal();
		return this.userRolesService.getUserRoles(userDetails.getUserId());
	}
}
