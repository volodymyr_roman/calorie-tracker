package fine.project.calorietracker.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

/**
 * JPA model that represents role records.
 *
 * @author Volodymyr Roman
 */
@Entity
@Table(name = "roles")
@Getter
@Setter
public class RoleModel {

	@Id
	@GeneratedValue
	private Long id;

	@Column(insertable = false)
	private String name;

	@Column(name = "permission", insertable = false)
	@ElementCollection(fetch = FetchType.LAZY)
	@CollectionTable(name = "user_permissions", joinColumns = @JoinColumn(name = "role_id"))
	private Set<String> permissions = new HashSet<>();
}
