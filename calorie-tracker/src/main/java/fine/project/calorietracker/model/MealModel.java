package fine.project.calorietracker.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;

/**
 * JPA model that represent meal records.
 *
 * @author Volodymyr Roman
 */
@Entity
@Table(name = "meals")
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class MealModel {

	@Id
	@GeneratedValue
	private Long id;

	@Column(columnDefinition = "DATE")
	private LocalDate date;

	@Column(columnDefinition = "TIME")
	private LocalTime time;

	@Column
	private String text;

	@Column
	private BigDecimal calories;

	@ManyToOne(optional = false)
	private UserModel user;

}
