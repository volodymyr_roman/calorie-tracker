package fine.project.calorietracker.mapper;

import fine.project.calorietracker.dto.UserDTO;
import fine.project.calorietracker.dto.UserResource;
import fine.project.calorietracker.model.UserModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

/**
 * Mapstruct mapper for users.
 *
 * @author Volodymyr Roman
 */
@Mapper(componentModel = "spring")
public interface UserMapper {

	/**
	 * Map from {@link UserDTO} to {@link UserModel}.
	 *
	 * @param userDTO source of mapping
	 * @return non null mapped object
	 */
	@Mapping(target = "id", ignore = true)
	UserModel userDtoToUserModel(UserDTO userDTO);

	/**
	 * Map from {@link UserModel} to {@link UserDTO}.
	 *
	 * @param userModel source of mapping
	 * @return non null mapped object
	 */
	UserResource userModelToUserResource(UserModel userModel);

	/**
	 * Update from {@link UserModel} with fields from {@link UserDTO}.
	 *
	 * @param userModel mapping target
	 * @param userDTO mapping source
	 */
	@Mapping(target = "id", ignore = true)
	void updateUser(@MappingTarget UserModel userModel, UserDTO userDTO);

}
