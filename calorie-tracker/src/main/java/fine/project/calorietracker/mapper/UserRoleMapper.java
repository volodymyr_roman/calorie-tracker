package fine.project.calorietracker.mapper;

import fine.project.calorietracker.dto.UserRolesResource;
import fine.project.calorietracker.model.RoleModel;
import fine.project.calorietracker.model.UserModel;
import org.mapstruct.Mapper;

import java.util.Set;
import java.util.stream.Collectors;

/**
 * Mapper for user roles.
 *
 * @author Volodymyr Roman
 */
@Mapper(componentModel = "spring")
public interface UserRoleMapper {

	/**
	 * Map user roles and permission.
	 */
	default UserRolesResource mapRoles(final UserModel userModel) {
		final UserRolesResource userRolesResource = new UserRolesResource();
		final Set<String> roles = userModel.getRoles().stream().map(RoleModel::getName).collect(Collectors.toSet());
		final Set<String> permissions = userModel.getRoles().stream().map(RoleModel::getPermissions).flatMap(
				Set::stream).collect(Collectors.toSet());
		userRolesResource.setPermissions(permissions);
		userRolesResource.setRoles(roles);
		return userRolesResource;
	}
}
