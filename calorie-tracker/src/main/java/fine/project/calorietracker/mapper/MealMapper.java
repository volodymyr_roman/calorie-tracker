package fine.project.calorietracker.mapper;

import fine.project.calorietracker.dto.MealDTO;
import fine.project.calorietracker.dto.MealResource;
import fine.project.calorietracker.model.MealModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;

/**
 * Mapstruct mapper for meals.
 *
 * @author Volodymyr Roman
 */
@Mapper(componentModel = "spring")
public interface MealMapper {

	/**
	 * Map from {@link MealDTO} to {@link MealModel}.
	 *
	 * @param mealDTO source of mapping
	 * @return non null mapped object
	 */
	@Mappings({
			@Mapping(target = "id", ignore = true),
			@Mapping(target = "user", ignore = true)
	})
	MealModel mealDtoToMealModel(MealDTO mealDTO);

	/**
	 * Map from {@link MealModel} to {@link MealDTO}.
	 *
	 * @param mealModel source of mapping
	 * @return non null mapped object
	 */
	@Mapping(target = "lessThanCalorieLimit", ignore = true)
	MealResource mealModelToMealResource(MealModel mealModel);

	/**
	 * Update from {@link MealModel} with fields from {@link MealDTO}.
	 *
	 * @param mealModel mapping target
	 * @param mealDTO mapping source
	 */
	@Mapping(target = "id", ignore = true)
	void updateMeal(@MappingTarget MealModel mealModel, MealDTO mealDTO);

}
