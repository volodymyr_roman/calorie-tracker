package fine.project.calorietracker.security;

import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

/**
 * Customize {@link User} for support {@link #userId} field.
 *
 * @author Volodymyr Roman
 */
public class CustomUserDetails extends User {

	@Getter
	private final Long userId;

	public CustomUserDetails(final String username, final String password,
			final Long userId,
			final Collection<? extends GrantedAuthority> authorities) {
		super(username, password, authorities);
		this.userId = userId;
	}
}
