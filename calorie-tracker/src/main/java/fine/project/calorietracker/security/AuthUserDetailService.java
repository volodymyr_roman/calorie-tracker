package fine.project.calorietracker.security;

import fine.project.calorietracker.dao.UserRepository;
import fine.project.calorietracker.model.RoleModel;
import fine.project.calorietracker.model.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.transaction.Transactional;

/**
 * Service that is used to load user for authentication.
 *
 * @author Volodymyr Roman
 */
@Component
public class AuthUserDetailService implements UserDetailsService {

	private final UserRepository userRepository;

	@Autowired
	public AuthUserDetailService(final UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Transactional
	@Override
	public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
		final UserModel userModel = this.userRepository
				.findByEmail(username)
				.orElseThrow(() -> new UsernameNotFoundException("User with name " + username + " no found"));

		final List<SimpleGrantedAuthority> roles = userModel.getRoles().stream().map(RoleModel::getName)
				.map(role -> "ROLE_" + role)
				.map(SimpleGrantedAuthority::new).collect(Collectors.toList());
		final List<SimpleGrantedAuthority> permissions = userModel.getRoles().stream()
				.flatMap(role -> role.getPermissions().stream())
				.distinct().map(SimpleGrantedAuthority::new).collect(Collectors.toList());

		final List<GrantedAuthority> authorities = new ArrayList<>(
				roles.size() + permissions.size());
		authorities.addAll(roles);
		authorities.addAll(permissions);

		return new CustomUserDetails(userModel.getEmail(), userModel.getPassword(),
				userModel.getId(), authorities);
	}
}
