package fine.project.calorietracker.security;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.provider.token.UserAuthenticationConverter;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Custom user converter is used to add additional claim user_id. On extraction from token create {@link
 * CustomUserDetails}.
 *
 * @author Volodymyr Roman
 */
public class CustomUserConverter implements UserAuthenticationConverter {

	private static final String USER_ID = "user_id";

	/**
	 * Put additional claim {@value #USER_ID} into response.
	 */
	@Override
	public Map<String, ?> convertUserAuthentication(final Authentication userAuthentication) {
		final Map<String, Object> response = new LinkedHashMap<>();
		response.put(USERNAME, userAuthentication.getName());
		response.put(AUTHORITIES, AuthorityUtils.authorityListToSet(userAuthentication.getAuthorities()));
		if (userAuthentication.getPrincipal() instanceof CustomUserDetails) {
			final CustomUserDetails principal = (CustomUserDetails) userAuthentication.getPrincipal();
			response.put(USER_ID, principal.getUserId());
		}
		return response;
	}

	/**
	 * Extract authentication and inject into it {@link CustomUserDetails}.
	 */
	@Override
	public Authentication extractAuthentication(final Map<String, ?> map) {
		final Object authorities = map.get(AUTHORITIES);
		List<GrantedAuthority> grantedAuthorities = null;
		if (authorities instanceof Collection) {
			grantedAuthorities = ((Collection<?>) authorities).stream().map(Object::toString)
					.map(SimpleGrantedAuthority::new)
					.collect(Collectors.toList());
		}
		final CustomUserDetails customUserDetails = new CustomUserDetails((String) map.get(USERNAME), "N/A",
				((Integer) map.get(USER_ID)).longValue(), grantedAuthorities);
		return new UsernamePasswordAuthenticationToken(customUserDetails, "N/A", grantedAuthorities);

	}
}
