package fine.project.calorietracker.service;

import fine.project.calorietracker.dto.MealDTO;
import fine.project.calorietracker.dto.MealResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Contains all operation with meals.
 *
 * @author Volodymyr Roman
 */
public interface MealService {

	/**
	 * Create meal for user.
	 */
	MealResource createMeal(Long userId, MealDTO mealDTO);

	/**
	 * Search meals with pagination. Supports such search request: <br/> <b>date eq '2016-05-01') AND
	 * ((number_of_calories gt 20) OR (number_of_calories lt 10))</b>
	 */
	Page<MealResource> searchMeals(Long userId, Pageable pageable, String search);

	/**
	 * Get meal for specific user.
	 *
	 * @param userId user id
	 * @param mealId meal id
	 * @return non null response
	 */
	MealResource getMeal(Long userId, Long mealId);

	/**
	 * Update meal for specific user.
	 *
	 * @param userId user id
	 * @param mealDTO meal that needs to be saved
	 * @param mealId meal id that will be updated
	 * @return non null response
	 */
	MealResource updateMeal(Long userId, Long mealId, MealDTO mealDTO);

	/**
	 * Delete meal for specific user.
	 *
	 * @param userId user id
	 * @param mealId meal id
	 */
	void deleteMeal(Long userId, Long mealId);
}
