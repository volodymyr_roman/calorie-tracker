package fine.project.calorietracker.service;

import fine.project.calorietracker.dto.UserRolesResource;

/**
 * User roles service.Support base operations with roles.
 *
 * @author Volodymyr Roman
 */
public interface UserRolesService {

	/**
	 * Add roles to user.
	 */
	UserRolesResource addRoleToUser(Long userId, String role);

	/**
	 * Delete role from user.But still user need to have at least 1 role.
	 */
	UserRolesResource deleteUserRole(Long userId, String role);

	/**
	 * Get user roes and permissions.
	 */
	UserRolesResource getUserRoles(Long userId);
}
