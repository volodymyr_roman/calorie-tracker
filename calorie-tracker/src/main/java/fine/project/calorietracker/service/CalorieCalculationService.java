package fine.project.calorietracker.service;

import java.math.BigDecimal;

/**
 * Calorie calculation service.
 *
 * @author Volodymyr Roman
 */
public interface CalorieCalculationService {

	/**
	 * Get calories by description.
	 */
	BigDecimal getCalories(String mealDescription);
}
