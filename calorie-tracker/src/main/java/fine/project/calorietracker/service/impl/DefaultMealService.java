package fine.project.calorietracker.service.impl;

import fine.project.calorietracker.dao.MealRepository;
import fine.project.calorietracker.dao.UserRepository;
import fine.project.calorietracker.dto.MealDTO;
import fine.project.calorietracker.dto.MealResource;
import fine.project.calorietracker.exception.ResourceNotFoundException;
import fine.project.calorietracker.mapper.MealMapper;
import fine.project.calorietracker.model.MealModel;
import fine.project.calorietracker.model.UserModel;
import fine.project.calorietracker.service.CalorieCalculationService;
import fine.project.calorietracker.service.MealService;
import fine.project.calorietracker.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;

/**
 * Base implementation of {@link MealService}.
 *
 * @author Volodymyr Roman
 */
@Service
public class DefaultMealService implements MealService {

	private final MealMapper mealMapper;

	private final MealRepository mealRepository;

	private final UserRepository userRepository;

	private final CalorieCalculationService calorieCalculationService;

	private final SearchService searchService;

	@Autowired
	public DefaultMealService(final MealMapper mealMapper, final MealRepository mealRepository,
			final UserRepository userRepository, final CalorieCalculationService calorieCalculationService,
			final SearchService searchService) {
		this.mealMapper = mealMapper;
		this.mealRepository = mealRepository;
		this.userRepository = userRepository;
		this.calorieCalculationService = calorieCalculationService;
		this.searchService = searchService;
	}

	@Override
	public MealResource createMeal(final Long userId, final MealDTO mealDTO) {
		final UserModel userModel = getUser(userId);
		final MealModel mealModel = this.mealMapper.mealDtoToMealModel(mealDTO);
		if (mealDTO.getCalories() == null) {
			mealModel.setCalories(this.calorieCalculationService.getCalories(mealDTO.getText()));
		}
		mealModel.setUser(userModel);
		final MealModel savedMeal = this.mealRepository.save(mealModel);
		final MealResource mealResource = this.mealMapper.mealModelToMealResource(savedMeal);
		checkCalories(mealResource, userModel);
		return mealResource;
	}

	@Override
	public Page<MealResource> searchMeals(final Long userId, final Pageable pageable, final String search) {
		final UserModel userModel = getUser(userId);
		final Page<MealModel> searchedMeals;
		if (search == null || search.isEmpty()) {
			searchedMeals = this.mealRepository.findAllByUser(userModel, pageable);
		} else {
			final Specifications<MealModel> userMealsQuery = Specifications.where(new UserPredicate(userModel)).and(
					this.searchService.buildSearchSpecification(search));
			searchedMeals = this.mealRepository.findAll(userMealsQuery, pageable);
		}
		final Page<MealResource> searchedMealResource = searchedMeals.map(this.mealMapper::mealModelToMealResource);
		searchedMealResource.forEach(meal -> checkCalories(meal, userModel));
		return searchedMealResource;
	}

	/**
	 * Predicate checks if meal belongs to current user.
	 */
	private class UserPredicate implements Specification<MealModel> {

		private final UserModel userModel;

		private UserPredicate(final UserModel userModel) {
			this.userModel = userModel;
		}

		@Override
		public Predicate toPredicate(final Root<MealModel> root, final CriteriaQuery<?> query,
				final CriteriaBuilder cb) {
			return cb.equal(root.get("user"), this.userModel);
		}
	}

	@Override
	public MealResource getMeal(final Long userId, final Long mealId) {
		final UserModel userModel = getUser(userId);
		final MealModel meal = getMeal(userModel, mealId);
		final MealResource mealResource = this.mealMapper.mealModelToMealResource(meal);
		checkCalories(mealResource, userModel);
		return mealResource;
	}

	@Override
	public MealResource updateMeal(final Long userId, final Long mealId, final MealDTO mealDTO) {
		final UserModel userModel = getUser(userId);
		final MealModel meal = getMeal(userModel, mealId);
		this.mealMapper.updateMeal(meal, mealDTO);
		final MealModel savedMeal = this.mealRepository.save(meal);
		final MealResource mealResource = this.mealMapper.mealModelToMealResource(savedMeal);
		checkCalories(mealResource, userModel);
		return mealResource;
	}

	@Override
	public void deleteMeal(final Long userId, final Long mealId) {
		final MealModel meal = getMeal(getUser(userId), mealId);
		this.mealRepository.delete(meal);
	}

	private MealModel getMeal(final UserModel userModel, final Long mealId) {
		return this.mealRepository.findByIdAndUser(mealId, userModel).orElseThrow(() -> new ResourceNotFoundException(
				"Meal with id = " + mealId + " doesn't belongs to user" + userModel.getId()));
	}

	private UserModel getUser(final Long userId) {
		return this.userRepository.findById(userId).orElseThrow(
				() -> new ResourceNotFoundException("User with id = " + userId + " doesn't exist"));
	}

	private void checkCalories(final MealResource mealResource, final UserModel userModel) {
		final BigDecimal caloriesPerDay = this.mealRepository.calculateCaloriesPerDay(mealResource.getDate(),
				userModel);
		if (userModel.getExpectedNumberOfCaloriesPerDay() != null) {
			if (caloriesPerDay == null) {
				mealResource.setLessThanCalorieLimit(true);
			} else {
				mealResource.setLessThanCalorieLimit(caloriesPerDay.
						compareTo(userModel.getExpectedNumberOfCaloriesPerDay()) <= 0);
			}
		}
	}
}
