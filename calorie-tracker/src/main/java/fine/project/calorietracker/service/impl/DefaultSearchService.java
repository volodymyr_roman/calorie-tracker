package fine.project.calorietracker.service.impl;

import fine.project.calorietracker.search.SearchPredicateBuilder;
import fine.project.calorietracker.search.SearchQuery;
import fine.project.calorietracker.search.SearchQueryBuilder;
import fine.project.calorietracker.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

/**
 * Base implementation of {@link SearchService}.
 *
 * @author Volodymyr Roman
 */
@Service
public class DefaultSearchService implements SearchService {

	private final SearchPredicateBuilder searchPredicateBuilder;

	private final SearchQueryBuilder searchQueryBuilder;

	@Autowired
	public DefaultSearchService(final SearchPredicateBuilder searchPredicateBuilder,
			final SearchQueryBuilder searchQueryBuilder) {
		this.searchPredicateBuilder = searchPredicateBuilder;
		this.searchQueryBuilder = searchQueryBuilder;
	}

	@Override
	public <T> Specification<T> buildSearchSpecification(final String searchString) {
		final SearchQuery searchQuery = this.searchQueryBuilder.build(searchString);
		return this.searchPredicateBuilder.buildPredicate(searchQuery);
	}
}
