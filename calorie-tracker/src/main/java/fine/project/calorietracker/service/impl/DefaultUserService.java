package fine.project.calorietracker.service.impl;

import fine.project.calorietracker.dao.MealRepository;
import fine.project.calorietracker.dao.RoleRepository;
import fine.project.calorietracker.dao.UserRepository;
import fine.project.calorietracker.dto.UserDTO;
import fine.project.calorietracker.dto.UserResource;
import fine.project.calorietracker.exception.ResourceNotFoundException;
import fine.project.calorietracker.mapper.UserMapper;
import fine.project.calorietracker.model.RoleModel;
import fine.project.calorietracker.model.UserModel;
import fine.project.calorietracker.service.SearchService;
import fine.project.calorietracker.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Base implementation of {@link UserService}.
 *
 * @author Volodymyr Roman
 */
@Service
public class DefaultUserService implements UserService {

	private final UserRepository userRepository;

	private final MealRepository mealRepository;

	private final UserMapper userMapper;

	private final PasswordEncoder passwordEncoder;

	private final RoleRepository roleRepository;

	private final SearchService searchService;

	private static final String USER_ROLE = "USER";

	@Autowired
	public DefaultUserService(final UserRepository userRepository, final MealRepository mealRepository,
			final UserMapper userMapper, final PasswordEncoder passwordEncoder, final RoleRepository roleRepository,
			final SearchService searchService) {
		this.userRepository = userRepository;
		this.mealRepository = mealRepository;
		this.userMapper = userMapper;
		this.passwordEncoder = passwordEncoder;
		this.roleRepository = roleRepository;
		this.searchService = searchService;
	}

	@Transactional
	@Override
	public UserResource createUser(final UserDTO userDTO) {
		final UserModel userModel = this.userMapper.userDtoToUserModel(userDTO);
		encodePassword(userDTO).ifPresent(userModel::setPassword);
		final RoleModel userRole = this.roleRepository.findByName(USER_ROLE).orElseThrow(() -> new RuntimeException(
				"User role '" + USER_ROLE + "' doesn't exist. Please contact administrator."));
		userModel.getRoles().add(userRole);
		final UserModel saveModel = this.userRepository.save(userModel);
		return this.userMapper.userModelToUserResource(saveModel);
	}

	@Override
	public UserResource getUser(final Long userId) {
		final UserModel user = getUserById(userId);
		return this.userMapper.userModelToUserResource(user);
	}

	@Override
	public Page<UserResource> searchUsers(final Pageable pageable, final String searchQuery) {
		final Page<UserModel> users;
		if (searchQuery == null || searchQuery.isEmpty()) {
			users = this.userRepository.findAll(pageable);
		} else {
			users = this.userRepository.findAll(this.searchService.buildSearchSpecification(searchQuery), pageable);
		}
		return users.map(this.userMapper::userModelToUserResource);
	}

	@Override
	@Transactional
	public UserResource updateUser(final Long userId, final UserDTO userDTO) {
		final UserModel user = getUserById(userId);
		this.userMapper.updateUser(user, userDTO);
		encodePassword(userDTO).ifPresent(user::setPassword);
		final UserModel savedUser = this.userRepository.save(user);
		return this.userMapper.userModelToUserResource(savedUser);
	}

	@Transactional
	@Override
	public void deleteUser(final Long userId) {
		final UserModel user = getUserById(userId);
		this.mealRepository.deleteAllByUser(user);
		this.userRepository.delete(user);
	}

	private UserModel getUserById(final Long userId) {
		return this.userRepository.findById(userId).orElseThrow(
				() -> new ResourceNotFoundException("User with id = " + userId + " doesn't exist"));
	}

	private Optional<String> encodePassword(final UserDTO userDTO) {
		final String password = userDTO.getPassword();
		Optional<String> encodedPassword = Optional.empty();
		if (password != null) {
			encodedPassword = Optional.of(this.passwordEncoder.encode(password));
		}
		return encodedPassword;
	}
}
