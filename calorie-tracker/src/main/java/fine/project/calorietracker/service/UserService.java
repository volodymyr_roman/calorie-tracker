package fine.project.calorietracker.service;

import fine.project.calorietracker.dto.UserDTO;
import fine.project.calorietracker.dto.UserResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Contains all operation with users.
 *
 * @author Volodymyr Roman
 */
public interface UserService {

	/**
	 * Create user.
	 *
	 * @param userDTO user that needs to be saved
	 * @return non null response
	 */

	UserResource createUser(UserDTO userDTO);

	/**
	 * Get user by id.
	 *
	 * @param userId user id
	 * @return non null response
	 */
	UserResource getUser(Long userId);

	/**
	 * Search users with pagination.
	 */
	Page<UserResource> searchUsers(Pageable pageable, String searchQuery);

	/**
	 * Update user.
	 *
	 * @param userId user id
	 * @param userDTO user info that needs to be updated
	 * @return non null response
	 */
	UserResource updateUser(Long userId, UserDTO userDTO);

	/**
	 * Delete user and all related records.
	 *
	 * @param userId user id
	 */
	void deleteUser(Long userId);
}
