package fine.project.calorietracker.service;

import org.springframework.data.jpa.domain.Specification;

/**
 * Provide functionality for search.
 *
 * @author Volodymyr Roman
 */
public interface SearchService {

	/**
	 * Build search specification from text.
	 */
	<T> Specification<T> buildSearchSpecification(String searchString);
}
