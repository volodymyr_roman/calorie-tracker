package fine.project.calorietracker.service;

import fine.project.calorietracker.model.RoleModel;

/**
 * Service to manage user roles.
 *
 * @author Volodymyr Roman
 */
public interface RolesService {

	/**
	 * Add role to existing user.
	 *
	 * @return added role.
	 */
	RoleModel addRoleToUser(Long userId, String userRole);

	/**
	 * Remove user role.
	 */
	void deleteUserRole(Long userId, String userRole);
}
