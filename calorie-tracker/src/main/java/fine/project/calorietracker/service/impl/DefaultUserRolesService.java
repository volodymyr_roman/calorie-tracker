package fine.project.calorietracker.service.impl;

import fine.project.calorietracker.dao.RoleRepository;
import fine.project.calorietracker.dao.UserRepository;
import fine.project.calorietracker.dto.UserRolesResource;
import fine.project.calorietracker.exception.DuplicateException;
import fine.project.calorietracker.exception.NotAllowedException;
import fine.project.calorietracker.exception.ResourceNotFoundException;
import fine.project.calorietracker.mapper.UserRoleMapper;
import fine.project.calorietracker.model.RoleModel;
import fine.project.calorietracker.model.UserModel;
import fine.project.calorietracker.service.UserRolesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * User roles service.Support base operations with roles.
 *
 * @author Volodymyr Roman
 */
@Service
public class DefaultUserRolesService implements UserRolesService {

	private final UserRoleMapper userRoleMapper;

	private final UserRepository userRepository;

	private final RoleRepository roleRepository;

	@Autowired
	public DefaultUserRolesService(final UserRoleMapper userRoleMapper, final UserRepository userRepository,
			final RoleRepository roleRepository) {
		this.userRoleMapper = userRoleMapper;
		this.userRepository = userRepository;
		this.roleRepository = roleRepository;
	}

	@Transactional
	@Override
	public UserRolesResource addRoleToUser(final Long userId, final String role) {
		final UserModel user = getUserById(userId);
		final RoleModel roleModel = this.roleRepository.findByName(role).orElseThrow(
				() -> new ResourceNotFoundException("Role '" + userId + "' doesn't exist"));
		if (user.getRoles().stream().map(RoleModel::getName).anyMatch(role::equals)) {
			throw new DuplicateException("Role '" + role + "' is already added to user.");
		}
		user.getRoles().add(roleModel);
		final UserModel savedUser = this.userRepository.save(user);
		return this.userRoleMapper.mapRoles(savedUser);
	}

	@Transactional
	@Override
	public UserRolesResource deleteUserRole(final Long userId, final String role) {
		final UserModel user = getUserById(userId);
		final RoleModel roleModel = this.roleRepository.findByName(role).orElseThrow(
				() -> new ResourceNotFoundException("Role '" + userId + "' doesn't exist"));
		if (!user.getRoles().remove(roleModel)) {
			throw new ResourceNotFoundException("User '" + userId + "' doesn't have role '" + role + "'");
		}
		if (user.getRoles().isEmpty()) {
			throw new NotAllowedException("User must contain at least 1 role");
		}
		final UserModel savedUser = this.userRepository.save(user);
		return this.userRoleMapper.mapRoles(savedUser);
	}

	@Transactional
	@Override
	public UserRolesResource getUserRoles(final Long userId) {
		return this.userRoleMapper.mapRoles(getUserById(userId));
	}

	private UserModel getUserById(final Long userId) {
		return this.userRepository.findById(userId).orElseThrow(
				() -> new ResourceNotFoundException("User with id = " + userId + " doesn't exist"));
	}

}
