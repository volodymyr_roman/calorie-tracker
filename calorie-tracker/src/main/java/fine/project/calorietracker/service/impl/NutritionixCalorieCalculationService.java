package fine.project.calorietracker.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import fine.project.calorietracker.exception.ThirdPartyComponentException;
import fine.project.calorietracker.exception.ValidationException;
import fine.project.calorietracker.service.CalorieCalculationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Iterator;

/**
 * Calorie calculation service base on https://www.nutritionix.com.
 *
 * @author Volodymyr Roman
 */
@Service
@Slf4j
public class NutritionixCalorieCalculationService implements CalorieCalculationService {

	private final RestTemplate restTemplate;

	private final String nutritionixUrl;

	private final ObjectMapper objectMapper;

	@Autowired
	public NutritionixCalorieCalculationService(final RestTemplate restTemplate,
			@Value("${nutritionix.url}") final String nutritionixUrl, final ObjectMapper objectMapper) {
		this.restTemplate = restTemplate;
		this.nutritionixUrl = nutritionixUrl;
		this.objectMapper = objectMapper;
	}

	/**
	 * Calculate calories by meal description.
	 *
	 * @param mealDescription meal description text.
	 * @return {@link BigDecimal#ZERO} if meal description is null
	 * @throws ValidationException if meal description text is not recognized
	 * @throws ThirdPartyComponentException if Nutritionix API is not avaliable
	 */
	@Override
	public BigDecimal getCalories(final String mealDescription) {
		if (mealDescription == null) {
			return BigDecimal.ZERO;
		}
		final String queryResponse;
		try {
			queryResponse = this.restTemplate.postForObject(this.nutritionixUrl,
					ImmutableMap.of("query", mealDescription), String.class);
		} catch (final HttpClientErrorException exception) {
			log.debug("Error while calling Nutritionix", exception);
			final HttpStatus statusCode = exception.getStatusCode();
			if (statusCode.is4xxClientError()) {
				throw new ValidationException("Not able to determine calories due to incorrect food description");
			} else {
				throw new ThirdPartyComponentException("Nutritionix API is not available");
			}
		}
		double calorieSum = 0;
		try {
			final JsonNode jsonNode = this.objectMapper.readTree(queryResponse);
			final Iterator<JsonNode> foods = jsonNode.get("foods").elements();
			while (foods.hasNext()) {
				calorieSum += foods.next().get("nf_calories").asDouble();
			}
		} catch (final IOException e) {
			log.error("Error while json parsing Nutritionix response {" + queryResponse + "}", e);
		}
		return BigDecimal.valueOf(calorieSum).setScale(2, BigDecimal.ROUND_HALF_UP);
	}
}
