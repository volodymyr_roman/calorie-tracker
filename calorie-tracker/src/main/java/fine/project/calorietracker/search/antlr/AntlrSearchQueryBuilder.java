package fine.project.calorietracker.search.antlr;

import fine.project.calorietracker.search.SearchQuery;
import fine.project.calorietracker.search.SearchQueryBuilder;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.springframework.stereotype.Component;
import parser.SearchQueryLexer;
import parser.SearchQueryParser;

/**
 * Uses ANTLR for parsing and syntax check.
 *
 * @author Volodymyr Roman
 */
@Component
public class AntlrSearchQueryBuilder implements SearchQueryBuilder {

	/**
	 * Build {@link SearchQuery} class from provided text.
	 */
	@Override
	public SearchQuery build(final String searchText) {
		final CharStream charStream = CharStreams.fromString(searchText);
		final SearchQueryLexer lexer = new SearchQueryLexer(charStream);
		final TokenStream tokens = new CommonTokenStream(lexer);
		final SearchQueryParser parser = new SearchQueryParser(tokens);
		parser.removeErrorListeners();
		parser.addErrorListener(ThrowingErrorListener.INSTANCE);
		final SearchQueryWalker classListener = new SearchQueryWalker();
		final ParseTreeWalker walker = new ParseTreeWalker();
		walker.walk(classListener, parser.searchRequestText());
		return classListener.getSearchQuery();
	}
}
