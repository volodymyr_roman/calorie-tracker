package fine.project.calorietracker.search;

import fine.project.calorietracker.exception.ValidationException;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * Defines base search criteria in db.
 *
 * @param <T> entity model type
 * @author Volodymyr Roman
 */
public class SearchSpecification<T> implements Specification<T> {

	private final QueryExpression queryExpression;

	private final ConversionService conversionService;

	public SearchSpecification(final QueryExpression queryExpression, final ConversionService conversionService) {
		this.queryExpression = queryExpression;
		this.conversionService = conversionService;
	}

	@Override
	public Predicate toPredicate(final Root<T> root, final CriteriaQuery<?> query, final CriteriaBuilder cb) {
		final Path<?> objectPath;
		try {
			objectPath = root.get(this.queryExpression.getField());
		} catch (final IllegalArgumentException e) {
			throw new ValidationException(
					"Field name in expression '" + this.queryExpression.getField() + "'" + " not exist", e);
		}
		final Object convert;
		try {
			convert = this.conversionService.convert(this.queryExpression.getValue(), objectPath.getJavaType());
		} catch (final Exception e) {
			throw new ValidationException(
					String.format("Incorrect field value type '%s' for field %s", this.queryExpression.getValue(),
							this.queryExpression.getField()), e);
		}
		final String operation = this.queryExpression.getOperation();
		final Predicate predicate;
		if (operation.equals(Operation.NOT_EQUALS.getOperationId())) {
			predicate = cb.notEqual(objectPath, convert);
		} else if (operation.equals(Operation.EQUALS.getOperationId())) {
			predicate = cb.equal(objectPath, convert);
		} else if (operation.equals(Operation.GRATER.getOperationId())) {
			predicate = cb.greaterThan((Path<Comparable>) objectPath, (Comparable) convert);
		} else if (operation.equals(Operation.LESS.getOperationId())) {
			predicate = cb.lessThan((Path<Comparable>) objectPath, (Comparable) convert);
		} else {
			throw new ValidationException(
					String.format("Operation '%s' is not supported.", this.queryExpression.getOperation()));
		}
		return predicate;
	}
}
