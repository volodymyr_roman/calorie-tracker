package fine.project.calorietracker.search;

/**
 * Build {@link SearchQuery} and check syntax of passed text.
 *
 * @author Volodymyr Roman
 */
public interface SearchQueryBuilder {

	/**
	 * Build {@link SearchQuery} class from provided text.
	 */
	SearchQuery build(String searchText);

}
