package fine.project.calorietracker.search;

import lombok.Getter;

import java.util.LinkedList;
import java.util.List;

/**
 * Parsed representation of search query.
 *
 * @author Volodymyr Roman
 */
public class SearchQuery {

	@Getter
	private final List<QueryExpression> queryExpressions = new LinkedList<>();

	@Getter
	private final List<QueryToken> queryTokens = new LinkedList<>();

	/**
	 * All supported tokens.
	 */
	public enum QueryToken {
		LEFT_PARANTESIS, RIGHT_PARANTESIS, AND, OR, EXPRESSION
	}

	/**
	 * Add query expression.
	 */
	public void addQueryExpression(final QueryExpression queryExpression) {
		this.queryExpressions.add(queryExpression);
	}

	/**
	 * Add token.
	 */
	public void addQueryToken(final QueryToken queryToken) {
		this.queryTokens.add(queryToken);
	}

}
