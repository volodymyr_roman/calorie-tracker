package fine.project.calorietracker.search;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

/**
 * Part of query. E.g city eq 'Lviv'
 *
 * @author Volodymyr Roman
 */
@AllArgsConstructor
@Getter
@ToString
@EqualsAndHashCode
public class QueryExpression {

	private String field;

	private String operation;

	private String value;
}
