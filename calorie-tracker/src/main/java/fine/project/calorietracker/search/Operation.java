package fine.project.calorietracker.search;

import lombok.Getter;

import java.util.Arrays;
import java.util.Optional;

/**
 * Operation enum.
 *
 * @author Volodyyr Roman
 */
public enum Operation {
	GRATER("gt"), LESS("lt"), EQUALS("eq"), NOT_EQUALS("ne");

	@Getter
	private final String operationId;

	Operation(final String operationId) {
		this.operationId = operationId;
	}

	/**
	 * Find operation by id.
	 */
	public static Optional<Operation> findById(final String id) {
		return Arrays.stream(values()).filter(o -> o.operationId.equals(id)).findAny();
	}
}
