package fine.project.calorietracker.search.impl;

import fine.project.calorietracker.search.QueryExpression;
import fine.project.calorietracker.search.SearchPredicateBuilder;
import fine.project.calorietracker.search.SearchQuery;
import fine.project.calorietracker.search.SearchSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Stack;
import java.util.function.Predicate;

/**
 * Default implementation of {@link SearchPredicateBuilder}.
 *
 * @author Volodymyr Roman
 */
@Component
public class DefaultSearchPredicateBuilder implements SearchPredicateBuilder {

	private final ConversionService conversionService;

	@Autowired
	public DefaultSearchPredicateBuilder(final ConversionService conversionService) {
		this.conversionService = conversionService;
	}

	/**
	 * Using Shunting-yard algorithm to build {@link Specification}.
	 */
	@Override
	public <T> Specification<T> buildPredicate(final SearchQuery searchQuery) {
		final Stack<Specification<T>> resultStack = new Stack<>();
		final Stack<SearchQuery.QueryToken> tokenStack = new Stack<>();
		final Iterator<SearchQuery.QueryToken> tokenIterator = searchQuery.getQueryTokens().iterator();
		final Iterator<QueryExpression> queryExpressionIterator = searchQuery.getQueryExpressions().iterator();
		while (tokenIterator.hasNext()) {
			final SearchQuery.QueryToken next = tokenIterator.next();
			if (next == SearchQuery.QueryToken.OR) {
				calculateResultWileLogicalOperators(tokenStack, resultStack);
				tokenStack.push(next);
			} else if (next == SearchQuery.QueryToken.AND) {
				// AND has the higher priority than OR
				calculateResultWileLogicalOperators(tokenStack, resultStack,
						ts -> ts.peek() == SearchQuery.QueryToken.AND);
				tokenStack.push(next);
			} else if (next == SearchQuery.QueryToken.EXPRESSION) {
				resultStack.push(new SearchSpecification<>(queryExpressionIterator.next(), this.conversionService));
			} else if (next == SearchQuery.QueryToken.LEFT_PARANTESIS) {
				tokenStack.push(next);
			} else if (next == SearchQuery.QueryToken.RIGHT_PARANTESIS) {
				calculateResultWileLogicalOperators(tokenStack, resultStack);
				tokenStack.pop();
			}
		}
		calculateResultWileLogicalOperators(tokenStack, resultStack);
		return resultStack.pop();
	}

	@SafeVarargs
	private final <T> void calculateResultWileLogicalOperators(final Stack<SearchQuery.QueryToken> tokenStack,
			final Stack<Specification<T>> resultStack,
			final Predicate<Stack<SearchQuery.QueryToken>>... additionalPredicates) {
		while (!tokenStack.isEmpty() && Arrays.stream(additionalPredicates).allMatch(
				stackPredicate -> stackPredicate.test(tokenStack)) &&
				(tokenStack.peek() == SearchQuery.QueryToken.AND || tokenStack.peek() == SearchQuery.QueryToken.OR)) {
			final Specification<T> right = resultStack.pop();
			final Specification<T> left = resultStack.pop();
			final SearchQuery.QueryToken pop = tokenStack.pop();
			if (pop == SearchQuery.QueryToken.AND) {
				resultStack.push(Specifications.where(left).and(right));
			} else if (pop == SearchQuery.QueryToken.OR) {
				resultStack.push(Specifications.where(left).or(right));
			}
		}
	}
}
