package fine.project.calorietracker.search.antlr;

import fine.project.calorietracker.search.QueryExpression;
import fine.project.calorietracker.search.SearchQuery;
import lombok.Getter;
import parser.SearchQueryBaseListener;
import parser.SearchQueryParser;

/**
 * Search query walker , goes throw all passed query and build convenient representation for further process.
 *
 * @author Volodymyr Roman
 */
public class SearchQueryWalker extends SearchQueryBaseListener {

	@Getter
	private final SearchQuery searchQuery;

	public SearchQueryWalker() {
		this.searchQuery = new SearchQuery();
	}

	@Override
	public void enterSearchExpression(final SearchQueryParser.SearchExpressionContext ctx) {
		this.searchQuery.addQueryExpression(
				new QueryExpression(ctx.field().getText(), ctx.operation().getText(), getFieldValue(ctx.fieldValue())));
		this.searchQuery.addQueryToken(SearchQuery.QueryToken.EXPRESSION);
	}

	private String getFieldValue(final SearchQueryParser.FieldValueContext fieldValueContext) {
		String fieldValue = null;
		if (fieldValueContext.fieldValueNumber() != null) {
			fieldValue = fieldValueContext.fieldValueNumber().getText();
		} else if (fieldValueContext.fieldValueString() != null) {
			fieldValue = encodeStringValue(fieldValueContext.fieldValueString().getText());
		}
		return fieldValue;
	}

	@Override
	public void enterLogicalOperationAND(final SearchQueryParser.LogicalOperationANDContext ctx) {
		this.searchQuery.addQueryToken(SearchQuery.QueryToken.AND);
	}

	@Override
	public void enterLogicalOperationOR(final SearchQueryParser.LogicalOperationORContext ctx) {
		this.searchQuery.addQueryToken(SearchQuery.QueryToken.OR);
	}

	@Override
	public void enterLeftParantesis(final SearchQueryParser.LeftParantesisContext ctx) {
		this.searchQuery.addQueryToken(SearchQuery.QueryToken.LEFT_PARANTESIS);
	}

	@Override
	public void enterRightParantesis(final SearchQueryParser.RightParantesisContext ctx) {
		this.searchQuery.addQueryToken(SearchQuery.QueryToken.RIGHT_PARANTESIS);
	}

	private String encodeStringValue(final String value) {
		return value.substring(1, value.length() - 1).replaceAll("\\\\'", "'");
	}
}
