package fine.project.calorietracker.search;

import org.springframework.data.jpa.domain.Specification;

/**
 * Build {@link Specification} from {@link SearchQuery}.
 *
 * @author Volodymyr Roman
 */
public interface SearchPredicateBuilder {

	/**
	 * Build {@link Specification} from text.
	 */
	<T> Specification<T> buildPredicate(SearchQuery searchQuery);
}
