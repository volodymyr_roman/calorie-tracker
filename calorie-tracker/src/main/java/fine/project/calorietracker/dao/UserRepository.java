package fine.project.calorietracker.dao;

import fine.project.calorietracker.model.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * MealModel repository.
 *
 * @author Volodymyr Roman
 */
@Repository
public interface UserRepository extends JpaRepository<UserModel, Long>, JpaSpecificationExecutor<UserModel> {

	/**
	 * Find user by id.
	 *
	 * @return {@link Optional#EMPTY} is there no such user
	 */
	Optional<UserModel> findById(Long id);

	/**
	 * Find user by email/username.
	 *
	 * @param email email/username
	 */
	Optional<UserModel> findByEmail(String email);
}
