package fine.project.calorietracker.dao;

import fine.project.calorietracker.model.MealModel;
import fine.project.calorietracker.model.UserModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;

/**
 * MealModel repository.
 *
 * @author Volodymyr Roman
 */
@Repository
public interface MealRepository extends JpaRepository<MealModel, Long>, JpaSpecificationExecutor<MealModel> {

	/**
	 * Calculate calories for passed user for certain day.
	 *
	 * @return sum of calories
	 */
	@Query(value = "SELECT sum(m.calories) FROM MealModel m WHERE m.date = ?1 and m.user = ?2")
	BigDecimal calculateCaloriesPerDay(LocalDate day, UserModel user);

	/**
	 * Find meal by id and restrict by user.
	 */
	Optional<MealModel> findByIdAndUser(Long id, UserModel userModel);

	/**
	 * Find all meal for user.
	 */
	Page<MealModel> findAllByUser(UserModel userModel, Pageable pageable);

	/**
	 * Delete all user meal.
	 */
	void deleteAllByUser(UserModel userModel);
}
