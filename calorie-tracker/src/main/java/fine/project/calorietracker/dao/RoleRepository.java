package fine.project.calorietracker.dao;

import fine.project.calorietracker.model.RoleModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * MealModel repository.
 *
 * @author Volodymyr Roman
 */
@Repository
public interface RoleRepository extends JpaRepository<RoleModel, Long> {

	/**
	 * Find role by name.
	 */
	Optional<RoleModel> findByName(String name);
}
