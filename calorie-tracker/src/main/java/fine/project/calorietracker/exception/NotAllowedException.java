package fine.project.calorietracker.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Notify attempting of creation duplicate resource.
 *
 * @author Volodymyr Roman
 */
@ResponseStatus(value = HttpStatus.CONFLICT)
public class NotAllowedException extends RuntimeException {

	public NotAllowedException(final String message) {
		super(message);
	}

	public NotAllowedException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public NotAllowedException(final Throwable cause) {
		super(cause);
	}

}
