package fine.project.calorietracker.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Notify attempting of creation duplicate resource.
 *
 * @author Volodymyr Roman
 */
@ResponseStatus(value = HttpStatus.CONFLICT)
public class DuplicateException extends RuntimeException {

	public DuplicateException(final String message) {
		super(message);
	}

	public DuplicateException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public DuplicateException(final Throwable cause) {
		super(cause);
	}

}
