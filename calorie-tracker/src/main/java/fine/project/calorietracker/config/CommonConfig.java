package fine.project.calorietracker.config;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

/**
 * Common config.
 *
 * @author Volodymyr Roman
 */
@Configuration
public class CommonConfig {

	/**
	 * Initialize rest template.
	 */
	@Bean
	RestTemplate restTemplate() {
		final HttpComponentsClientHttpRequestFactory clientHttpRequestFactory
				= new HttpComponentsClientHttpRequestFactory();
		clientHttpRequestFactory.setHttpClient(httpClient());
		return new RestTemplate(clientHttpRequestFactory);
	}

	/**
	 * Create apache http client.
	 */
	@Bean
	HttpClient httpClient() {
		return HttpClients.createDefault();
	}
}
