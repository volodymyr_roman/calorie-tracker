package fine.project.calorietracker.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;

/**
 * Resource server configuration.In future we can split resource server and authorization server.
 *
 * @author Volodymyr Roman
 */
@Configuration
@EnableResourceServer
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

	@Autowired
	private ResourceServerTokenServices tokenServices;

	@Value("${security.jwt.resource-ids}")
	private String resourceIds;

	@Override
	public void configure(final ResourceServerSecurityConfigurer resources) {
		resources
				.resourceId(this.resourceIds)
				.tokenServices(this.tokenServices);
	}

	@Override
	public void configure(final HttpSecurity http) throws Exception {
		http
				.authorizeRequests()
				.antMatchers("/api/users").permitAll()
				.antMatchers("/api/**").authenticated()
				.anyRequest().permitAll();
	}
}
