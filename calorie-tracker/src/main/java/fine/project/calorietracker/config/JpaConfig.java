package fine.project.calorietracker.config;

import fine.project.calorietracker.CalorieTrackerApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

/**
 * Configuration for JPA. May contains different customizations.
 *
 * @author Volodymyr Roman
 */
@Configuration
@EntityScan(basePackageClasses = {CalorieTrackerApplication.class, Jsr310JpaConverters.class})
public class JpaConfig {

}
