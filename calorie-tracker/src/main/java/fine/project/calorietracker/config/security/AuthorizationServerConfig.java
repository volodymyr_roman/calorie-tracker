package fine.project.calorietracker.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

/**
 * Authorization server configuration.For now we are only using password grant type.
 *
 * @author Volodymyr Roman
 */
@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

	@Value("${security.jwt.client-id}")
	private String clientId;

	@Value("${security.jwt.client-secret}")
	private String clientSecret;

	@Value("${security.jwt.resource-ids}")
	private String resourceIds;

	@Value("${security.jwt.access-token.validity-seconds}")
	private int accessTokenValiditySeconds;

	@Value("${security.jwt.refresh-token.validity-seconds}")
	private int refreshTokenValiditySeconds;

	@Autowired
	private UserDetailsService userDetailsService;

	@Autowired
	private TokenStore tokenStore;

	@Autowired
	@Qualifier("accessTokenConverter")
	private JwtAccessTokenConverter jwtAccessTokenConverter;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Override
	public void configure(final ClientDetailsServiceConfigurer configurer) throws Exception {
		configurer
				.inMemory()
				.withClient(this.clientId)
				.secret(this.clientSecret)
				.scopes("read", "write")
				.authorizedGrantTypes("password", "refresh_token")
				.accessTokenValiditySeconds(this.accessTokenValiditySeconds)
				.refreshTokenValiditySeconds(this.refreshTokenValiditySeconds)
				.resourceIds(this.resourceIds);
	}

	@Override
	public void configure(final AuthorizationServerEndpointsConfigurer endpoints) {
		endpoints
				.userDetailsService(this.userDetailsService)
				.tokenEnhancer(this.jwtAccessTokenConverter)
				.tokenStore(this.tokenStore)
				.authenticationManager(this.authenticationManager)
				.accessTokenConverter(this.jwtAccessTokenConverter)
				.reuseRefreshTokens(false);
	}

}
