package fine.project.calorietracker.config.swagger;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Redirect to Swagger UI.
 *
 * @author Volodymyr Roman
 */
@ApiIgnore(value = "Technical endpoint for redirect")
@Controller
public class IndexController {

	/**
	 * Redirect to swagger ui page.
	 *
	 * @return redirect response
	 */
	@GetMapping(value = "/")
	String swaggerUrl() {
		return "redirect:/swagger-ui.html";
	}
}
