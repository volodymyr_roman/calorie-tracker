package fine.project.calorietracker.config;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

/**
 * Configuration for Jackson.
 *
 * @author Volodymyr Roman
 */
@Configuration
public class JacksonConfig {

	/**
	 * Create and configure {@link ObjectMapper}.
	 */
	@Bean
	ObjectMapper objectMapper() {
		final Jackson2ObjectMapperBuilder jackson2ObjectMapperBuilder = new Jackson2ObjectMapperBuilder();
		jackson2ObjectMapperBuilder.failOnUnknownProperties(true);
		jackson2ObjectMapperBuilder.serializationInclusion(Include.NON_NULL);
		jackson2ObjectMapperBuilder.featuresToDisable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
		jackson2ObjectMapperBuilder.featuresToDisable(SerializationFeature.WRITE_NULL_MAP_VALUES);
		jackson2ObjectMapperBuilder.modules(new JavaTimeModule());
		return jackson2ObjectMapperBuilder.build();
	}
}
