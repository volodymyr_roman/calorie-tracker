package fine.project.calorietracker.config.swagger;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.time.LocalTime;

/**
 * Configuration class allowing to define Swagger API for the project.
 *
 * @author Volodymyr Roman
 */
@EnableSwagger2
@Configuration
@ConfigurationProperties(value = "calorietracker.swagger")
@Getter
@Setter
public class SwaggerConfiguration {

	private String applicationName;

	private String title;

	private String description;

	private String version;

	private String contactName;

	private String contactUrl;

	private String contactEmail;

	/**
	 * Create {@link Docket} bean and configure swagger configuration base on properties. .
	 *
	 * @return {@link Docket}
	 */
	@Bean
	public Docket newsApi() {
		final Docket docket = new Docket(DocumentationType.SWAGGER_2).groupName(this.applicationName).apiInfo(apiInfo())
				.useDefaultResponseMessages(false).select()
				.apis(RequestHandlerSelectors.basePackage("fine.project.calorietracker"))
				.paths(PathSelectors.any()).build();
		//Swagger can't visualize correct LocalTime
		docket.directModelSubstitute(LocalTime.class, String.class);
		return docket;
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()
				.title(this.title)
				.description(this.description)
				.contact(new Contact(this.contactName, this.contactUrl, this.contactEmail))
				.version(this.version)
				.build();
	}

}
