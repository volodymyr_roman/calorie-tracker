package fine.project.calorietracker.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;

/**
 * DTO that is used for create and update operations.
 *
 * @author Volodymyr Roman
 */
@Getter
@Setter
public class MealDTO {

	@NotNull
	@ApiModelProperty(example = "2017-12-31")
	private LocalDate date;

	@NotNull
	@ApiModelProperty(example = "12:30")
	private LocalTime time;

	@NotNull
	@Size(min = 2)
	@ApiModelProperty(example = "1 egg")
	private String text;

	@Min(value = 1)
	@ApiModelProperty(dataType = "double", example = "100")
	private BigDecimal calories;
}
