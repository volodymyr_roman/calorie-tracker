package fine.project.calorietracker.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

/**
 * DTO that is used for create and update operations.
 *
 * @author Volodymyr Roman
 */
@Getter
@Setter
@ToString
public class UserDTO {

	@NotNull
	@Size(min = 2, max = 30)
	@ApiModelProperty(example = "Volodymyr")
	private String firstName;

	@Size(min = 2, max = 30)
	@ApiModelProperty(example = "Roman")
	private String lastName;

	@Email(message = "Email address is not valid")
	@ApiModelProperty(example = "vova.rova@gmail.com")
	private String email;

	@NotNull
	@Size(min = 4, max = 30)
	@ApiModelProperty(example = "vova.rova@gmail.com")
	private String password;

	@Min(value = 100)
	@ApiModelProperty(dataType = "float", example = "700")
	private BigDecimal expectedNumberOfCaloriesPerDay;
}
