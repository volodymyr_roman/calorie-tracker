package fine.project.calorietracker.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

/**
 * User roles and permission DTO.
 *
 * @author Volodymyr Roman
 */
@Getter
@Setter
public class UserRolesResource {

	@ApiModelProperty
	private Set<String> roles;

	@ApiModelProperty
	private Set<String> permissions;
}
