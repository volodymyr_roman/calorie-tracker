package fine.project.calorietracker.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * DTO that is used to return information.
 *
 * @author Volodymyr Roman
 */
@Getter
@Setter
public class UserResource {

	@ApiModelProperty(example = "1")
	private Long id;

	@ApiModelProperty(example = "Volodymyr")
	private String firstName;

	@ApiModelProperty(example = "Roman")
	private String lastName;

	@ApiModelProperty(example = "vova.rova@gmail.com")
	private String email;

	@ApiModelProperty(dataType = "double", example = "700")
	private BigDecimal expectedNumberOfCaloriesPerDay;
}
