package fine.project.calorietracker.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;

/**
 * DTO that is used to return information.
 *
 * @author Volodymyr Roman
 */
@Getter
@Setter
public class MealResource {

	private Long id;

	@ApiModelProperty(example = "2017-12-31")
	private LocalDate date;

	@ApiModelProperty(example = "12:30")
	private LocalTime time;

	@ApiModelProperty(example = "1 egg")
	private String text;

	private BigDecimal calories;

	private Boolean lessThanCalorieLimit;
}
