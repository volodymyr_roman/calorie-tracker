package fine.project.calorietracker.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

/**
 * Customize existing converters to support format 2017-12-31.
 *
 * @author Volodymyr Roman
 */
@Component
public class StringToLocalDateConverter implements Converter<String, LocalDate> {

	@Override
	public LocalDate convert(final String source) {
		return source == null ? null : LocalDate.parse(source);
	}
}
