package fine.project.calorietracker.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.LocalTime;

/**
 * Customize existing converters to support format 2017-12-31.
 *
 * @author Volodymyr Roman
 */
@Component
public class StringToLocalTimeConverter implements Converter<String, LocalTime> {

	@Override
	public LocalTime convert(final String source) {
		return source == null ? null : LocalTime.parse(source);
	}
}
