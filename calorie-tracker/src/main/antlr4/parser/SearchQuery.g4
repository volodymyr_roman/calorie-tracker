grammar SearchQuery;
searchRequestText:searchRequest EOF;
searchRequest : searchExpression
                |leftParantesis searchRequest rightParantesis
				|searchRequest (logicalOperationAND|logicalOperationOR)  searchRequest;
searchExpression :field ' ' operation ' ' fieldValue;
fieldValue :fieldValueString|fieldValueNumber;
fieldValueString :QUOTED_STRING;
leftParantesis: '(';
rightParantesis: ')';
fieldValueNumber :DIGIT+ ('.'? DIGIT+)?;
operation :WORD+;
field : (WORD|DIGIT)+;
logicalOperationAND :' AND ';
logicalOperationOR :' OR ';
WORD : [a-zA-Z];
DIGIT: [0-9];
fragment ESCAPED_QUOTE : '\\\'';
QUOTED_STRING :   '\'' ( ESCAPED_QUOTE | ~('\n'|'\r') )*? '\'';
