package fine.project.calorietracker.converters;

import org.junit.Assert;
import org.junit.Test;

import java.time.LocalTime;
import java.time.format.DateTimeParseException;

public class StringToLocalTimeConverterTest {

	StringToLocalTimeConverter stringToLocalTimeConverter = new StringToLocalTimeConverter();

	@Test
	public void convert() throws Exception {
		final LocalTime convertedTime = this.stringToLocalTimeConverter.convert("23:30:12");
		Assert.assertEquals(23, convertedTime.getHour());
		Assert.assertEquals(30, convertedTime.getMinute());
		Assert.assertEquals(12, convertedTime.getSecond());
	}

	@Test(expected = DateTimeParseException.class)
	public void convertInvalidData() throws Exception {
		this.stringToLocalTimeConverter.convert("20170301");
	}

	@Test
	public void convertNullString() throws Exception {
		Assert.assertNull(this.stringToLocalTimeConverter.convert(null));
	}
}