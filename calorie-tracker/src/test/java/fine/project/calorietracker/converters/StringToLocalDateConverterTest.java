package fine.project.calorietracker.converters;

import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

public class StringToLocalDateConverterTest {

	StringToLocalDateConverter stringToLocalDateConverter = new StringToLocalDateConverter();

	@Test
	public void convert() throws Exception {
		final LocalDate convertedDate = this.stringToLocalDateConverter.convert("2017-03-01");
		Assert.assertEquals(1, convertedDate.getDayOfMonth());
		Assert.assertEquals(3, convertedDate.getMonthValue());
		Assert.assertEquals(2017, convertedDate.getYear());
	}

	@Test(expected = DateTimeParseException.class)
	public void convertInvalidData() throws Exception {
		this.stringToLocalDateConverter.convert("20170301");
	}

	@Test
	public void convertNullString() throws Exception {
		Assert.assertNull(this.stringToLocalDateConverter.convert(null));
	}
}