package fine.project.calorietracker.service.impl;

import fine.project.calorietracker.dao.MealRepository;
import fine.project.calorietracker.dao.UserRepository;
import fine.project.calorietracker.dto.MealDTO;
import fine.project.calorietracker.dto.MealResource;
import fine.project.calorietracker.model.UserModel;
import fine.project.calorietracker.service.MealService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDate;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DefaultMealServiceTest {

	@Autowired
	MealService mealService;

	@Autowired
	UserRepository userRepository;

	@Autowired
	MealRepository mealRepository;

	private Long userId;

	@Before
	@Transactional
	public void setUp() throws Exception {
		final UserModel user = new UserModel();
		user.setEmail("em1");
		user.setExpectedNumberOfCaloriesPerDay(BigDecimal.valueOf(300));
		user.setFirstName("user");
		user.setLastName("last");
		this.userId = this.userRepository.saveAndFlush(user).getId();
	}

	@After
	public void tearDown() throws Exception {
		this.mealRepository.deleteAllByUser(this.userRepository.findOne(this.userId));
		this.userRepository.delete(this.userId);
	}

	@Test
	@Transactional
	public void createMeal() throws Exception {
		final MealDTO mealDTO = new MealDTO();
		mealDTO.setDate(LocalDate.now());
		mealDTO.setCalories(BigDecimal.valueOf(400));
		final MealResource meal = this.mealService.createMeal(this.userId, mealDTO);
		Assert.assertNotNull(this.mealRepository.findOne(meal.getId()));
		Assert.assertFalse(meal.getLessThanCalorieLimit());
		Assert.assertEquals(mealDTO.getCalories(), meal.getCalories());
		Assert.assertEquals(mealDTO.getDate(), meal.getDate());
		this.mealRepository.delete(meal.getId());
	}

	@Test
	@Transactional
	public void createMeaWithoutExistingEmail() throws Exception {
		final MealDTO mealDTO = new MealDTO();
		mealDTO.setDate(LocalDate.now());
		mealDTO.setText("1 egg");
		final MealResource meal = this.mealService.createMeal(this.userId, mealDTO);
		Assert.assertNotNull(this.mealRepository.findOne(meal.getId()));
		Assert.assertTrue(meal.getLessThanCalorieLimit());
		Assert.assertEquals(mealDTO.getDate(), meal.getDate());
		Assert.assertEquals(mealDTO.getText(), meal.getText());
		Assert.assertEquals(BigDecimal.valueOf(72), meal.getCalories().setScale(0, BigDecimal.ROUND_HALF_UP));
		this.mealRepository.delete(meal.getId());
	}
}
