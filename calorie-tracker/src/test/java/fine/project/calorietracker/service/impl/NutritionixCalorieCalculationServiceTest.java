package fine.project.calorietracker.service.impl;

import fine.project.calorietracker.exception.ValidationException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

@RunWith(SpringRunner.class)
@SpringBootTest
public class NutritionixCalorieCalculationServiceTest {

	@Autowired
	NutritionixCalorieCalculationService nutritionixCalorieCalculationService;

	@Test
	public void getCalories() {
		final BigDecimal calories = this.nutritionixCalorieCalculationService.getCalories(
				"1 cup mashed potatoes and 2 tbsp gravy");
		Assert.assertEquals(251, Math.round(calories.doubleValue()));
	}

	@Test(expected = ValidationException.class)
	public void getCaloriesEmptyRequest() {
		this.nutritionixCalorieCalculationService.getCalories("");
	}

	@Test(expected = ValidationException.class)
	public void getCaloriesInvalidRequest() {
		this.nutritionixCalorieCalculationService.getCalories("ee");
	}
}
