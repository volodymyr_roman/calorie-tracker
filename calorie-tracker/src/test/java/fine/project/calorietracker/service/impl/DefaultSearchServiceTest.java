package fine.project.calorietracker.service.impl;

import fine.project.calorietracker.search.SearchPredicateBuilder;
import fine.project.calorietracker.search.SearchQuery;
import fine.project.calorietracker.search.SearchQueryBuilder;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.data.jpa.domain.Specification;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

public class DefaultSearchServiceTest {

	@Test
	public void buildSearchSpecification() throws Exception {
		final SearchPredicateBuilder searchPredicateBuilder = mock(SearchPredicateBuilder.class);
		final SearchQueryBuilder searchQueryBuilder = mock(SearchQueryBuilder.class);
		final SearchQuery searchQuery = new SearchQuery();
		given(searchQueryBuilder.build("SOMETEXT")).willReturn(searchQuery);
		final Specification specification = mock(Specification.class);
		given(searchPredicateBuilder.buildPredicate(searchQuery)).willReturn(specification);
		final DefaultSearchService defaultSearchService = new DefaultSearchService(searchPredicateBuilder,
				searchQueryBuilder);
		final Specification<Object> resultSpecification = defaultSearchService.buildSearchSpecification("SOMETEXT");
		Assert.assertEquals(specification, resultSpecification);
	}

}