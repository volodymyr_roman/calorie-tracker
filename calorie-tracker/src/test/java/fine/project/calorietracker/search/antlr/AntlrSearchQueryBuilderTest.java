package fine.project.calorietracker.search.antlr;

import fine.project.calorietracker.search.QueryExpression;
import fine.project.calorietracker.search.SearchQuery;
import org.antlr.v4.runtime.misc.ParseCancellationException;
import org.junit.Assert;
import org.junit.Test;

public class AntlrSearchQueryBuilderTest {

	@Test(expected = ParseCancellationException.class)
	public void buildSyntaxErrorTest() throws Exception {
		final AntlrSearchQueryBuilder antlrSearchQueryBuilder = new AntlrSearchQueryBuilder();
		antlrSearchQueryBuilder.build("f1 eq '21' ADN 1");
	}

	@Test(expected = ParseCancellationException.class)
	public void buildSyntaxErrorTestWithUnescaped() throws Exception {
		final AntlrSearchQueryBuilder antlrSearchQueryBuilder = new AntlrSearchQueryBuilder();
		antlrSearchQueryBuilder.build("f1 eq ''21' ADN 1");
	}

	@Test
	public void buildValidSyntaxTest() throws Exception {
		final AntlrSearchQueryBuilder antlrSearchQueryBuilder = new AntlrSearchQueryBuilder();
		final SearchQuery searchQuery = antlrSearchQueryBuilder.build(
				"(field1AND eq '21') AND (fieldTwo gt 210.32) OR fieldThree eq 'TREE'");
		Assert.assertEquals(3, searchQuery.getQueryExpressions().size());
		Assert.assertEquals(new QueryExpression("field1AND", "eq", "21"), searchQuery.getQueryExpressions().get(0));
		Assert.assertEquals(new QueryExpression("fieldTwo", "gt", "210.32"), searchQuery.getQueryExpressions().get(1));
		Assert.assertEquals(new QueryExpression("fieldThree", "eq", "TREE"), searchQuery.getQueryExpressions().get(2));

		Assert.assertEquals(9, searchQuery.getQueryTokens().size());
		Assert.assertEquals(SearchQuery.QueryToken.LEFT_PARANTESIS, searchQuery.getQueryTokens().get(0));
		Assert.assertEquals(SearchQuery.QueryToken.EXPRESSION, searchQuery.getQueryTokens().get(1));
		Assert.assertEquals(SearchQuery.QueryToken.RIGHT_PARANTESIS, searchQuery.getQueryTokens().get(2));
		Assert.assertEquals(SearchQuery.QueryToken.AND, searchQuery.getQueryTokens().get(3));
		Assert.assertEquals(SearchQuery.QueryToken.LEFT_PARANTESIS, searchQuery.getQueryTokens().get(4));
		Assert.assertEquals(SearchQuery.QueryToken.EXPRESSION, searchQuery.getQueryTokens().get(5));
		Assert.assertEquals(SearchQuery.QueryToken.RIGHT_PARANTESIS, searchQuery.getQueryTokens().get(6));
		Assert.assertEquals(SearchQuery.QueryToken.OR, searchQuery.getQueryTokens().get(7));
		Assert.assertEquals(SearchQuery.QueryToken.EXPRESSION, searchQuery.getQueryTokens().get(8));
	}

	@Test
	public void checkEscape() throws Exception {
		final AntlrSearchQueryBuilder antlrSearchQueryBuilder = new AntlrSearchQueryBuilder();
		final String searchText =
				"(field1AND eq '\\'#@#@SAAAEEJI\\'JXZnkxz\\'') AND (fieldTwo gt 210.32) OR fieldThree eq '\\''";
		final SearchQuery searchQuery = antlrSearchQueryBuilder.build(searchText);
		Assert.assertEquals(3, searchQuery.getQueryExpressions().size());
		Assert.assertEquals(new QueryExpression("field1AND", "eq", "'#@#@SAAAEEJI'JXZnkxz'"),
				searchQuery.getQueryExpressions().get(0));
		Assert.assertEquals(new QueryExpression("fieldTwo", "gt", "210.32"), searchQuery.getQueryExpressions().get(1));
		Assert.assertEquals(new QueryExpression("fieldThree", "eq", "'"), searchQuery.getQueryExpressions().get(2));

		Assert.assertEquals(9, searchQuery.getQueryTokens().size());
		Assert.assertEquals(SearchQuery.QueryToken.LEFT_PARANTESIS, searchQuery.getQueryTokens().get(0));
		Assert.assertEquals(SearchQuery.QueryToken.EXPRESSION, searchQuery.getQueryTokens().get(1));
		Assert.assertEquals(SearchQuery.QueryToken.RIGHT_PARANTESIS, searchQuery.getQueryTokens().get(2));
		Assert.assertEquals(SearchQuery.QueryToken.AND, searchQuery.getQueryTokens().get(3));
		Assert.assertEquals(SearchQuery.QueryToken.LEFT_PARANTESIS, searchQuery.getQueryTokens().get(4));
		Assert.assertEquals(SearchQuery.QueryToken.EXPRESSION, searchQuery.getQueryTokens().get(5));
		Assert.assertEquals(SearchQuery.QueryToken.RIGHT_PARANTESIS, searchQuery.getQueryTokens().get(6));
		Assert.assertEquals(SearchQuery.QueryToken.OR, searchQuery.getQueryTokens().get(7));
		Assert.assertEquals(SearchQuery.QueryToken.EXPRESSION, searchQuery.getQueryTokens().get(8));
	}
}