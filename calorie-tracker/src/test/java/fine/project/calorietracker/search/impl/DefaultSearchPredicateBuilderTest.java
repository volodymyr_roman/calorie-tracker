package fine.project.calorietracker.search.impl;

import fine.project.calorietracker.dao.UserRepository;
import fine.project.calorietracker.model.UserModel;
import fine.project.calorietracker.search.QueryExpression;
import fine.project.calorietracker.search.SearchPredicateBuilder;
import fine.project.calorietracker.search.SearchQuery;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DefaultSearchPredicateBuilderTest {

	@Autowired
	UserRepository userRepository;

	@Autowired
	SearchPredicateBuilder searchPredicateBuilder;

	UserModel savedUser1;

	UserModel savedUser2;

	UserModel savedUser3;

	@Before
	@Transactional
	public void setUp() throws Exception {
		UserModel user = new UserModel();
		user.setEmail("em1");
		user.setExpectedNumberOfCaloriesPerDay(BigDecimal.ZERO);
		user.setFirstName("user");
		user.setLastName("last");
		this.savedUser1 = this.userRepository.save(user);

		user = new UserModel();
		user.setEmail("em2");
		user.setExpectedNumberOfCaloriesPerDay(BigDecimal.ONE);
		user.setFirstName("user");
		user.setLastName("last2");
		this.savedUser2 = this.userRepository.save(user);

		user = new UserModel();
		user.setEmail("em3");
		user.setExpectedNumberOfCaloriesPerDay(BigDecimal.TEN);
		user.setFirstName("user3");
		user.setLastName("last3");
		this.savedUser3 = this.userRepository.save(user);
	}

	@After
	@Transactional
	public void tearDown() throws Exception {
		this.userRepository.delete(this.savedUser1);
		this.userRepository.delete(this.savedUser2);
		this.userRepository.delete(this.savedUser3);
	}

	@Transactional
	@Test
	public void buildSimplePredicate() throws Exception {
		final SearchQuery searchQuery = new SearchQuery();
		searchQuery.addQueryExpression(new QueryExpression("email", "eq", "em1"));
		searchQuery.addQueryExpression(new QueryExpression("email", "eq", "em2"));
		searchQuery.addQueryToken(SearchQuery.QueryToken.LEFT_PARANTESIS);
		searchQuery.addQueryToken(SearchQuery.QueryToken.EXPRESSION);
		searchQuery.addQueryToken(SearchQuery.QueryToken.RIGHT_PARANTESIS);
		searchQuery.addQueryToken(SearchQuery.QueryToken.OR);
		searchQuery.addQueryToken(SearchQuery.QueryToken.LEFT_PARANTESIS);
		searchQuery.addQueryToken(SearchQuery.QueryToken.EXPRESSION);
		searchQuery.addQueryToken(SearchQuery.QueryToken.RIGHT_PARANTESIS);
		final List<UserModel> searchResult = this.userRepository.findAll(
				this.searchPredicateBuilder.buildPredicate(searchQuery));
		Assert.assertEquals(2, searchResult.size());
		Assert.assertEquals(this.savedUser1, searchResult.get(0));
		Assert.assertEquals(this.savedUser2, searchResult.get(1));
	}

	@Transactional
	@Test
	public void buildPredicateWithoutResult() throws Exception {
		final SearchQuery searchQuery = new SearchQuery();
		searchQuery.addQueryExpression(new QueryExpression("email", "eq", "em1"));
		searchQuery.addQueryExpression(new QueryExpression("email", "eq", "em2"));
		searchQuery.addQueryToken(SearchQuery.QueryToken.LEFT_PARANTESIS);
		searchQuery.addQueryToken(SearchQuery.QueryToken.EXPRESSION);
		searchQuery.addQueryToken(SearchQuery.QueryToken.RIGHT_PARANTESIS);
		searchQuery.addQueryToken(SearchQuery.QueryToken.AND);
		searchQuery.addQueryToken(SearchQuery.QueryToken.LEFT_PARANTESIS);
		searchQuery.addQueryToken(SearchQuery.QueryToken.EXPRESSION);
		searchQuery.addQueryToken(SearchQuery.QueryToken.RIGHT_PARANTESIS);
		final List<UserModel> searchResult = this.userRepository.findAll(
				this.searchPredicateBuilder.buildPredicate(searchQuery));
		Assert.assertEquals(0, searchResult.size());
	}

	@Transactional
	@Test
	public void buildComplexPredicate() throws Exception {
		final SearchQuery searchQuery = new SearchQuery();
		searchQuery.addQueryExpression(new QueryExpression("email", "eq", "em1"));
		searchQuery.addQueryExpression(new QueryExpression("email", "eq", "em2"));
		searchQuery.addQueryExpression(new QueryExpression("email", "eq", "em3"));
		searchQuery.addQueryExpression(new QueryExpression("expectedNumberOfCaloriesPerDay", "lt", "1"));
		searchQuery.addQueryExpression(new QueryExpression("expectedNumberOfCaloriesPerDay", "gt", "5"));

		searchQuery.addQueryToken(SearchQuery.QueryToken.LEFT_PARANTESIS);
		searchQuery.addQueryToken(SearchQuery.QueryToken.EXPRESSION);
		searchQuery.addQueryToken(SearchQuery.QueryToken.OR);
		searchQuery.addQueryToken(SearchQuery.QueryToken.EXPRESSION);
		searchQuery.addQueryToken(SearchQuery.QueryToken.OR);
		searchQuery.addQueryToken(SearchQuery.QueryToken.EXPRESSION);
		searchQuery.addQueryToken(SearchQuery.QueryToken.RIGHT_PARANTESIS);

		searchQuery.addQueryToken(SearchQuery.QueryToken.AND);

		searchQuery.addQueryToken(SearchQuery.QueryToken.LEFT_PARANTESIS);
		searchQuery.addQueryToken(SearchQuery.QueryToken.EXPRESSION);
		searchQuery.addQueryToken(SearchQuery.QueryToken.OR);
		searchQuery.addQueryToken(SearchQuery.QueryToken.EXPRESSION);
		searchQuery.addQueryToken(SearchQuery.QueryToken.RIGHT_PARANTESIS);

		final List<UserModel> searchResult = this.userRepository.findAll(
				this.searchPredicateBuilder.buildPredicate(searchQuery));
		Assert.assertEquals(2, searchResult.size());
		Assert.assertEquals(this.savedUser1, searchResult.get(0));
		Assert.assertEquals(this.savedUser3, searchResult.get(1));
	}

}