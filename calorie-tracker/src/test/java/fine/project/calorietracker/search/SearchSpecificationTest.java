package fine.project.calorietracker.search;

import fine.project.calorietracker.dao.MealRepository;
import fine.project.calorietracker.dao.UserRepository;
import fine.project.calorietracker.exception.ValidationException;
import fine.project.calorietracker.model.MealModel;
import fine.project.calorietracker.model.UserModel;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.convert.ConversionService;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SearchSpecificationTest {

	@Autowired
	MealRepository mealRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	ConversionService conversionService;

	UserModel savedUser;

	MealModel mealModel1;

	MealModel mealModel2;

	@Before
	@Transactional
	public void setUp() throws Exception {
		this.savedUser = this.userRepository.save(new UserModel());
		final LocalDate now = LocalDate.now();
		MealModel mealModel = new MealModel();
		mealModel.setUser(this.savedUser);
		mealModel.setText("Vova");
		mealModel.setDate(now);
		mealModel.setCalories(BigDecimal.TEN);
		this.mealModel1 = this.mealRepository.saveAndFlush(mealModel);

		mealModel = new MealModel();
		mealModel.setUser(this.savedUser);
		mealModel.setText("Text");
		mealModel.setDate(now);
		mealModel.setCalories(BigDecimal.ONE);
		this.mealModel2 = this.mealRepository.saveAndFlush(mealModel);
	}

	@After
	@Transactional
	public void tearDown() throws Exception {
		this.mealRepository.delete(this.mealModel1);
		this.mealRepository.delete(this.mealModel2);
		this.userRepository.delete(this.savedUser);
	}

	@Test
	@Transactional
	public void testEqSearch() throws Exception {
		final SearchSpecification<MealModel> searchSpecification = new SearchSpecification<>(
				new QueryExpression("text", "eq", "Vova"), this.conversionService);

		final List<MealModel> all = this.mealRepository.findAll(searchSpecification);
		Assert.assertEquals(1, all.size());
		Assert.assertEquals(this.mealModel1, all.iterator().next());
	}

	@Test
	@Transactional
	public void testNotEquals() throws Exception {
		final SearchSpecification<MealModel> searchSpecification = new SearchSpecification<>(
				new QueryExpression("text", "ne", "Vova"), this.conversionService);

		final List<MealModel> all = this.mealRepository.findAll(searchSpecification);
		Assert.assertEquals(1, all.size());
		Assert.assertEquals(this.mealModel2, all.iterator().next());
	}

	@Test
	@Transactional
	public void testLessThan() throws Exception {
		final SearchSpecification<MealModel> searchSpecification = new SearchSpecification<>(
				new QueryExpression("calories", "lt", "4"), this.conversionService);

		final List<MealModel> all = this.mealRepository.findAll(searchSpecification);
		Assert.assertEquals(1, all.size());
		Assert.assertEquals(this.mealModel2, all.iterator().next());
	}

	@Test
	@Transactional
	public void testGraterThan() throws Exception {
		final SearchSpecification<MealModel> searchSpecification = new SearchSpecification<>(
				new QueryExpression("calories", "gt", "4"), this.conversionService);

		final List<MealModel> all = this.mealRepository.findAll(searchSpecification);
		Assert.assertEquals(1, all.size());
		Assert.assertEquals(this.mealModel1, all.iterator().next());
	}

	@Test(expected = ValidationException.class)
	public void testInvalidOperator() throws Exception {
		final SearchSpecification<MealModel> searchSpecification = new SearchSpecification<>(
				new QueryExpression("calories", "INVALID", "4"), this.conversionService);
		this.mealRepository.findAll(searchSpecification);
	}

	@Test(expected = ValidationException.class)
	public void testInvalidField() throws Exception {
		final SearchSpecification<MealModel> searchSpecification = new SearchSpecification<>(
				new QueryExpression("INVALID", "lt", "4"), this.conversionService);
		this.mealRepository.findAll(searchSpecification);
	}

	@Test(expected = ValidationException.class)
	public void testInvalidFieldValue() throws Exception {
		final SearchSpecification<MealModel> searchSpecification = new SearchSpecification<>(
				new QueryExpression("calories", "lt", "dsdsd"), this.conversionService);
		this.mealRepository.findAll(searchSpecification);
	}

}
