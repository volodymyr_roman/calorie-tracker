package fine.project.calorietracker.mapper;

import fine.project.calorietracker.dto.UserRolesResource;
import fine.project.calorietracker.model.RoleModel;
import fine.project.calorietracker.model.UserModel;
import org.junit.Assert;
import org.junit.Test;
import org.mapstruct.factory.Mappers;

import java.util.Collections;

public class UserRoleMapperTest {

	UserRoleMapper userRoleMapper = Mappers.getMapper(UserRoleMapper.class);

	@Test
	public void mapRoles() throws Exception {
		final UserModel userModel = new UserModel();
		final RoleModel admin = new RoleModel();
		admin.setName("ADMIN");
		admin.setPermissions(Collections.singleton("P1"));
		userModel.getRoles().add(admin);
		final UserRolesResource userRolesResource = this.userRoleMapper.mapRoles(userModel);
		Assert.assertEquals(1, userRolesResource.getRoles().size());
		Assert.assertEquals("ADMIN", userRolesResource.getRoles().iterator().next());
		Assert.assertEquals(1, userRolesResource.getPermissions().size());
		Assert.assertEquals("P1", userRolesResource.getPermissions().iterator().next());
	}

	@Test
	public void mapRolesEmpytRoles() throws Exception {
		final UserModel userModel = new UserModel();
		final UserRolesResource userRolesResource = this.userRoleMapper.mapRoles(userModel);
		Assert.assertEquals(0, userRolesResource.getRoles().size());
		Assert.assertEquals(0, userRolesResource.getPermissions().size());
	}

}