package fine.project.calorietracker.mapper;

import fine.project.calorietracker.dto.MealDTO;
import fine.project.calorietracker.dto.MealResource;
import fine.project.calorietracker.model.MealModel;
import org.junit.Assert;
import org.junit.Test;
import org.mapstruct.factory.Mappers;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;

public class MealMapperTest {

	private final MealMapper mealMapper = Mappers.getMapper(MealMapper.class);

	@Test
	public void mealDtoToMealModel() throws Exception {
		final MealDTO mealDTO = new MealDTO();
		mealDTO.setCalories(BigDecimal.TEN);
		mealDTO.setDate(LocalDate.now());
		mealDTO.setTime(LocalTime.now());
		mealDTO.setText("TXT");
		final MealModel mealModel = this.mealMapper.mealDtoToMealModel(mealDTO);
		Assert.assertEquals(mealDTO.getCalories(), mealModel.getCalories());
		Assert.assertEquals(mealDTO.getDate(), mealModel.getDate());
		Assert.assertEquals(mealDTO.getText(), mealModel.getText());
		Assert.assertEquals(mealDTO.getTime(), mealModel.getTime());
		Assert.assertNull(mealModel.getUser());
	}

	@Test
	public void mealModelToMealResource() throws Exception {
		final MealModel mealModel = new MealModel();
		mealModel.setCalories(BigDecimal.TEN);
		mealModel.setDate(LocalDate.now());
		mealModel.setTime(LocalTime.now());
		mealModel.setText("TXT");
		final MealResource mealResource = this.mealMapper.mealModelToMealResource(mealModel);
		Assert.assertEquals(mealModel.getCalories(), mealResource.getCalories());
		Assert.assertEquals(mealModel.getDate(), mealResource.getDate());
		Assert.assertEquals(mealModel.getText(), mealResource.getText());
		Assert.assertEquals(mealModel.getTime(), mealResource.getTime());
		Assert.assertNull(mealResource.getLessThanCalorieLimit());
	}

	@Test
	public void updateMeal() throws Exception {
		final MealModel mealModel = new MealModel();
		mealModel.setCalories(BigDecimal.ONE);
		mealModel.setDate(LocalDate.now().minusDays(1));
		mealModel.setTime(LocalTime.now().minusHours(1));
		mealModel.setText("SOMETEXT");

		final MealDTO mealDTO = new MealDTO();
		mealDTO.setCalories(BigDecimal.TEN);
		mealDTO.setDate(LocalDate.now());
		mealDTO.setTime(LocalTime.now());
		mealDTO.setText("TXT");

		this.mealMapper.updateMeal(mealModel, mealDTO);
		Assert.assertEquals(mealDTO.getCalories(), mealModel.getCalories());
		Assert.assertEquals(mealDTO.getDate(), mealModel.getDate());
		Assert.assertEquals(mealDTO.getText(), mealModel.getText());
		Assert.assertEquals(mealDTO.getTime(), mealModel.getTime());
		Assert.assertNull(mealModel.getUser());
	}

}