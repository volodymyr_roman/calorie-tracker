package fine.project.calorietracker.mapper;

import fine.project.calorietracker.dto.UserDTO;
import fine.project.calorietracker.dto.UserResource;
import fine.project.calorietracker.model.UserModel;
import org.junit.Assert;
import org.junit.Test;
import org.mapstruct.factory.Mappers;

import java.math.BigDecimal;

public class UserMapperTest {

	private final UserMapper userMapper = Mappers.getMapper(UserMapper.class);

	@Test
	public void userDtoToUserModel() throws Exception {
		final UserDTO userDTO = new UserDTO();
		userDTO.setEmail("email");
		userDTO.setExpectedNumberOfCaloriesPerDay(BigDecimal.TEN);
		userDTO.setFirstName("vromnan");
		userDTO.setLastName("roman");
		userDTO.setPassword("PP");

		final UserModel userModel = this.userMapper.userDtoToUserModel(userDTO);
		Assert.assertEquals(userDTO.getEmail(), userModel.getEmail());
		Assert.assertEquals(userDTO.getExpectedNumberOfCaloriesPerDay(),
				userModel.getExpectedNumberOfCaloriesPerDay());
		Assert.assertEquals(userDTO.getFirstName(), userModel.getFirstName());
		Assert.assertEquals(userDTO.getLastName(), userModel.getLastName());
		Assert.assertEquals(userDTO.getPassword(), userModel.getPassword());
		Assert.assertTrue(userModel.getRoles().isEmpty());
		Assert.assertNull(userModel.getId());
	}

	@Test
	public void userModelToUserResource() throws Exception {
		final UserModel userModel = new UserModel();
		userModel.setEmail("email");
		userModel.setExpectedNumberOfCaloriesPerDay(BigDecimal.TEN);
		userModel.setFirstName("vromnan");
		userModel.setLastName("roman");
		userModel.setPassword("PP");
		userModel.setId(1L);

		final UserResource userResource = this.userMapper.userModelToUserResource(userModel);
		Assert.assertEquals(userModel.getEmail(), userResource.getEmail());
		Assert.assertEquals(userModel.getExpectedNumberOfCaloriesPerDay(),
				userResource.getExpectedNumberOfCaloriesPerDay());
		Assert.assertEquals(userModel.getFirstName(), userResource.getFirstName());
		Assert.assertEquals(userModel.getLastName(), userResource.getLastName());
		Assert.assertEquals(userModel.getId(), userResource.getId());
	}

	@Test
	public void updateUser() throws Exception {
		final UserDTO userDTO = new UserDTO();
		userDTO.setEmail("email");
		userDTO.setExpectedNumberOfCaloriesPerDay(BigDecimal.TEN);
		userDTO.setFirstName("vromnan");
		userDTO.setLastName("roman");
		userDTO.setPassword("PP");

		final UserModel userModel = new UserModel();
		userModel.setEmail("email");
		userModel.setExpectedNumberOfCaloriesPerDay(BigDecimal.TEN);
		userModel.setFirstName("vromnan");
		userModel.setLastName("roman");
		userModel.setPassword("PP");
		userModel.setId(1L);

		this.userMapper.updateUser(userModel, userDTO);

		Assert.assertEquals(userDTO.getEmail(), userModel.getEmail());
		Assert.assertEquals(userDTO.getExpectedNumberOfCaloriesPerDay(),
				userModel.getExpectedNumberOfCaloriesPerDay());
		Assert.assertEquals(userDTO.getFirstName(), userModel.getFirstName());
		Assert.assertEquals(userDTO.getLastName(), userModel.getLastName());
		Assert.assertEquals(userDTO.getPassword(), userModel.getPassword());
		Assert.assertNotNull(userModel.getRoles());
		Assert.assertNotNull(userModel.getId());
	}

}